<?php
class PDOManager
{
    private static $instance = null;
    protected $pdo;
    private function __construct(){}
    private function __clone(){}
    public static function getInstance()
    {
    	if(is_null(self::$instance))
    	self::$instance = new self;
    	return self::$instance;
    }
    
    public function getPdo()
    {
        $connect = array(
		'host' 		=> 'localhost',
		'type' 		=> 'mysql',
		'dbname' 	=> 'lokisalle',
		'user'		=> 'root',
		'password'	=> ''
        );
        
        try
	{
            $this->pdo = new PDO("mysql:dbname=" . $connect['dbname'] . ";host=" . $connect['host'], $connect['user'], $connect['password'], array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
	}
	catch(PDOException $e)
	{
            'Echec connexion : ' . $e->getMessage();
	}
	return $this->pdo;
    }
    
}