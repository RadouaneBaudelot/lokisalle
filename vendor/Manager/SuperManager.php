<?php
class SuperManager
{
    /** @var PDOManager */
    protected $db;

    public function __construct()
    {
        $this->db = $this->getDb();
    }
    
    /**
     * 
     * @return type
     */
    public function getDb()
    {
    	if(!$this->db)
    	{
            $this->db = PDOManager::getInstance()->getPdo();
            return $this->db;
	}
	return $this->db;
    }
    
    /**
     * 
     * @param type $entity
     * @param type $param
     * @param type $property
     * @return type
     */
    public function findOneBy($entity, $param, $property)
    {
        $req = $this->db->prepare("SELECT * FROM $entity WHERE $param= :property");
        $req->bindValue(':property',  $property,    PDO::PARAM_STR);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->fetchObject(ucfirst($entity));
        $req->closeCursor();
        
        return $result;
    }
    
    public function findBy($entity, $param, $property)
    {
        $req = $this->db->prepare("SELECT * FROM $entity WHERE $param= :property");
        $req->bindValue(':property',  $property,    PDO::PARAM_STR);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->fetchAll(PDO::FETCH_CLASS, ucfirst($entity));
        $req->closeCursor();
        
        return $result;
    }
    
    public function findById($entity, $property) {
        $id = 'id' . ucfirst($entity);
        $req = $this->db->prepare("SELECT * FROM $entity WHERE $id = :property");
        $req->bindValue(':property',  $property,    PDO::PARAM_STR);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $req->setFetchMode(PDO::FETCH_CLASS, ucfirst($entity));
        $result = $req->fetch();
        $req->closeCursor();
        
    return $result;
    }
    
    public function getAll($entity)
    {
        $req = $this->db->prepare("SELECT * FROM $entity");
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->fetchAll(PDO::FETCH_CLASS, ucfirst($entity));
        $req->closeCursor();
        
        return $result;
    }
    
    public function delete($entity, $param, $property)
    {
        $req = $this->db->prepare("DELETE FROM $entity
                                   WHERE $param= :property");
        $req->bindValue(':property',  $property,    PDO::PARAM_STR);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $req->closeCursor();
    }
    
    public function getIdByCriteria($entity, $param, $property)
    {
        $id = 'id' . ucfirst($entity);
        $req = $this->db->prepare("SELECT $id FROM $entity WHERE $param= :property");
        $req->bindValue(':property',  $property,    PDO::PARAM_STR);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->fetch();
        $req->closeCursor();
        
        return (int)$result[0];
    }
}

/*class PDOManager
{
    private static $instance = null;
    protected $pdo;
    private function __construct(){}
    private function __clone(){}
    public static function getInstance()
    {
    	if(is_null(self::$instance))
    	self::$instance = new self;
    	return self::$instance;
    }
    
    public function getPdo()
    {
        $connect = array(
		'host' 		=> 'localhost',
		'type' 		=> 'mysql',
		'dbname' 	=> 'lokisalle',
		'user'		=> 'root',
		'password'	=> ''
        );
        //include __DIR__ . '/../../app/Config.php';

        //$config = new Config();
        //$connect = $config->getParametersConnect();
        try
	{
            $this->pdo = new PDO("mysql:dbname=" . $connect['dbname'] . ";host=" . $connect['host'], $connect['user'], $connect['password'], array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
	}
	catch(PDOException $e)
	{
            'Echec connexion : ' . $e->getMessage();
	}
	return $this->pdo;
    }
    
}*/

/*
PDO::FETCH_CLASS permet une instanciation à partir de la classe désiré
SetFetchMode : permet de remplir les propriétés de cet objet avc le résultat avec SQL
*/