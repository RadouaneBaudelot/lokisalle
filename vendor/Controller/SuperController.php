<?php
class SuperController
{
    protected $title;
    protected $action;
    
    public function getManager()
    {
    	$class = ucfirst($_GET['controller']).'Manager';
        return $manager = new $class;
    }
    
    public function getHandler()
    {
        $class = ucfirst($_GET['controller']).'Handler';
        return $handler = new $class;
    }
    
    public function getEntity()
    {
        $class = ucfirst($_GET['controller']);
        return $entity = new $class;
    }
    
    public function render($path, $arg)
    {
        extract($arg);
        ob_start();
        require_once RACINE_FILE . PROJECT . '/src/View/View'.$path.'.php';
        $buffer = ob_get_contents();
        ob_get_clean();
        require_once RACINE_FILE . PROJECT . '/src/View/layout.php';
    }
    
    public function getTitle()
    {
        $className = get_called_class();
        return $this->title = str_replace('Controller', '', $className);
    }
    
    public function getAction()
    {
        if(isset($_GET['action'])) {
            return $this->action = ucfirst($_GET['action']);
        } else {
            return $this->action = '';
        }
    }
    
    public function redirect($controller, $action)
    {
        header("Location: " . RACINE_SITE . "/$controller/$action");
    }
}
