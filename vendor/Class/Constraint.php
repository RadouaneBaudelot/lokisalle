<?php

class Constraint
{
    public function maxLength($property, $length)
    {
        if (strlen($property) >= $length) {
            return 'Ce champs ne doit pas dépasser ' . $length . ' caractères. ';
        }
    }
    
    public function minLength($property, $length)
    {
        if (strlen($property) <= $length) {
            return 'Ce champs doit comporter au minimum ' . $length . ' caractères ';
        }
    }
    
    public function isNotEmpty($property)
    {
        if (empty($property)) {
            return 'Ce champs est obligatoire. ';
        }
    }
    
    public function isValidCp($cp)
    {
        if(!preg_match("#[^0-9]#", $cp)) {
            if (strlen($cp) !== 5) {
                return 'Le Code Postal doit être composé de 5 chiffres. ';
            }
        } else {
            return 'Le Code Postal doit être composé de 5 chiffres. ';
        }
    }
    
    public function isValidSexe($sexe)
    {
        if($sexe !== 'm' && $sexe !== 'f')
        {
            return 'Veuillez cocher l\'une des 2 cases : Homme ou Femme';
        }
    }
    
    public function isEmailCorrect($email)
    {
        // vérification de la validité de l'email (format alias@domaine.extension
        if (preg_match("#^[a-z0-9+._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $email)) {
            return FALSE;
        } else {
            return 'Veuillez saisir un format d\'email valide du type : identifiant@domaine.ext';
        }
    }
    
    public function pseudoExists($pseudo)
    {
        $membreManager = new MembreManager;
        if ($membreManager->pseudoExists($pseudo)) {
            return TRUE;
        }
    }
    
    public function emailExists($email)
    {
        // vérification de l'existence de l'email sais en base de donnée : pas de doublon d'adresse email autorisé
        $membreManager = new MembreManager;
        if ($membreManager->emailExists($email)) {
            return TRUE;
        }
    }
    
    public function isValidCategorie($categorie)
    {
        if($categorie !== 'cat1' && $categorie !== 'cat2')
        {
            return 'Veuillez cocher l\'une des 2 cases : Cat1 ou Cat2';
        }
    }
    
    public function isValidDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}
