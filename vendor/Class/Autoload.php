<?php
class Autoload {
    public static function className($className)
    {
        $className = ucfirst($className);
        
        $directories = array('src', 'vendor');
        
        foreach($directories as $dir)
        {
            if(file_exists(RACINE_FILE.'/'.PROJECT.'/'.$dir.'/Class/'.$className.'.php')) {                
                $folder = $dir;
                $file   = 'Class';
                $extension = '';
            } elseif(file_exists(RACINE_FILE.'/'.PROJECT.'/'.$dir.'/Entity/'.$className.'.php')) {                
                $folder = $dir;
                $file   = 'Entity';
                $extension = '';
            } elseif(file_exists(RACINE_FILE.'/'.PROJECT.'/'.$dir.'/Email/'.$className.'.php')) {                
                $folder = $dir;
                $file   = 'Email';
                $extension = '';
            } elseif(file_exists(RACINE_FILE.'/'.PROJECT.'/'.$dir.'/Handler/'.$className.'.php')) {                
                $folder = $dir;
                $file   = 'Handler';
                $extension = '';
            } elseif(file_exists(RACINE_FILE.'/'.PROJECT.'/'.$dir.'/Controller/'.$className.'.php')) {
                $folder = $dir;
                $file   = 'Controller';
                $extension = '';
            } elseif(file_exists(RACINE_FILE.'/'.PROJECT.'/'.$dir.'/Manager/'.$className.'.php')) {
                $folder = $dir;
                $file   = 'Manager';
                $extension = '';
                
                if($className == 'PDOManager') { // Pour aller chercher la classe PDOManager qui n'a pas un nom formaté
                    $extension = '';
                }
            }
        }

        if(file_exists(RACINE_FILE.'/'.PROJECT.'/'.$folder.'/'.$file.'/'.$className.$extension.'.php')) {
            require RACINE_FILE.'/'.PROJECT.'/'.$folder.'/'.$file.'/'.$className.$extension.'.php';
        }
    }
}
spl_autoload_register(array('Autoload', 'className'));