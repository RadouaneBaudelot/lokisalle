<?php

class Upload
{
    public function checkExtension($file)
    {
        $extension = strrchr($_FILES[$file]['name'], '.');
        $extension = strtolower(substr($extension, 1));
        $tabValidExtension = array('gif', 'jpg', 'jpeg', 'png');
        $checkExtension = in_array($extension, $tabValidExtension);
        
        return $checkExtension;
    }
    
    public function uploadFileinDb($file, $salle)
    {
        if($this->checkExtension($file)) {
            $name = $salle.'_'.$_FILES[$file]['name'];
            return $fileDb = RACINE_SITE."/img/salle/$name";
        }
    }
    
    public function saveFileInPath($file, $salle)
    {
        if($this->checkExtension($file)) {
            $name = $salle.'_'.$_FILES[$file]['name'];
            $fileProject = RACINE_FILE.PROJECT."/web/img/salle/$name";
            copy($_FILES[$file]['tmp_name'], $fileProject);
        }
    }
}

