<?php
/*
Class Email

construire les différents email dans des méthodes
liste des email :
    - récupération du mot de passe
  
 * Pensée : faire une classe email builder qui créé les email
 * faire une classe email qui envoie les emails  
*/

class EmailBuilder
{
    protected $text;
    protected $boundary;
    protected $error = 0;
    protected $errMessage;
    
    public function __construct()
    {
        $this->boundary = "-----=".md5(rand());
    }
    
    public function getText() {
        return $this->text;
    }

    public function setText($text) {
        $this->text = $text;
    }
    
    public function prepareHeader($email, $sender, $name)
    {
        $passageLigne = $this->getPassageLigne($email);
        
        $header = "From: \"$name\"<$sender>".$passageLigne;
        $header.= "Reply-to: \"$name\" <$sender>".$passageLigne;
        $header.= "MIME-Version: 1.0".$passageLigne;
        $header.= "Content-Type: multipart/alternative;".$passageLigne." boundary=\"$this->boundary\"".$passageLigne;
        
        return $header;
    }
    
    public function prepareMessage($email)
    {
        $passageLigne = $this->getPassageLigne($email);
        
        $message = $passageLigne."--".$this->boundary.$passageLigne;
        $message.= "Content-Type: text/html; charset=\"utf-8\"".$passageLigne;
        $message.= "Content-Transfer-Encoding: 8bit".$passageLigne;
        $message.= $passageLigne.$this->getText()."<br/>".$passageLigne;
        $message.= $passageLigne."--".$this->boundary."--".$passageLigne;
        
        return $message;
    }
    
    public function sendEmail($email, $subject ,$message, $header)
    {
        if (!empty($email)) { // Si l'email existe
            if (!$this->checkEmailFormat($email)) { // Si l'adresse email est valide
                $this->error++;
                $this->errMessage = 'L\'email doit être valide; exemple : nom@domain.fr';
            }
        } else {
        $this->error++;
        $this->errMessage = 'L\'email est obligatoire';
        }
        
        if ($this->error === 0) {
             mail($email, $subject, $message, $header);
        }
    }
        
    private function checkEmailFormat($email)
    {
        $syntaxe='#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';  
        if(preg_match($syntaxe,$email)) {
            return true;  
        } else {
            return false;
        }
    }
    
    private function getPassageLigne($email)
    {
        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $email)) { // On filtre les serveurs qui rencontrent des bogues.
            return "\r\n";
        } else {
            return "\n";
        }
    }
}