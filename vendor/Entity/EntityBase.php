<?php

class EntityBase
{
    public function __construct($data = null)
    {
        $this->hydrate($data);
    }
    
    public function hydrate($data)
    {
        foreach ($data as $key => $value)
        {
            $method = 'set'.ucfirst($key);
            
            if (method_exists($this, $method))
            {
                $this->$method($value);
            }
        }
    }
}
