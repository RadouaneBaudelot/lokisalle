<?php

class ProduitHandler
{
    // Properties declaration     
    protected $constraint;
    protected $produit;
    protected $produitManager;
    protected $error;
    protected $errDateArrivee;
    protected $errDateDepart;
    protected $errPrix;
    protected $errSalle;
    protected $errPromo;
    
    /**
     * Instanciation of objects used in this class
     */
    public function __construct()
    {
        $this->produit = new Produit();
        $this->constraint = new Constraint();
        $this->produitManager = new ProduitManager();
    }
    
    /**
     * Check All Datas sent in registration Form
     * 
     * @return boolean
     * TRUE if no errors
     * FALSE if errors
     */
    public function checkDatasForCreation($idSalle, $salleOption)
    {
        // Intialize error property
        $this->error = 0;
        
        // check form validity
        $this->checkDateArrivee($this->produit->getDateArrivee());
        $this->checkDateDepart($this->produit->getDateDepart());
        $this->checkDates($this->produit->getDateArrivee(), $this->produit->getDateDepart(), $idSalle);
        $this->checkPrix($this->produit->getPrix());
        $this->checkSalle($salleOption);
        
        // Check if errors has been returned during checks
        if ($this->error == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function getErrDateArrivee() {
        return $this->errDateArrivee;
    }

    public function getErrDateDepart() {
        return $this->errDateDepart;
    }

    public function getErrPrix() {
        return $this->errPrix;
    }

    public function getErrSalle() {
        return $this->errSalle;
    }

    public function getErrPromo() {
        return $this->errPromo;
    }

    private function checkDateArrivee($dateArrivee)
    {
        if ($this->constraint->isNotEmpty($dateArrivee)) {
            $this->errDateArrivee = $this->constraint->isNotEmpty($dateArrivee);
            return $this->error++;
        }
        
        if ($this->constraint->isValidDate($dateArrivee)) {
            $this->errDateArrivee = 'Le format de date n\'est pas valide : merci de saisir une date au forma jj/mm/aaa';
            return $this->error++;
        }
    }
    
    private function checkDateDepart($dateDepart)
    {
        if ($this->constraint->isNotEmpty($dateDepart)) {
            $this->errDateDepart = $this->constraint->isNotEmpty($dateDepart);
            return $this->error++;
        }
        
        if ($this->constraint->isValidDate($dateDepart)) {
            $this->errDateDepart = 'Le format de date n\'est pas valide : merci de saisir une date au forma jj/mm/aaa';
            return $this->error++;
        }
    }
    
    private function checkDates($dateArrivee, $dateDepart, $idSalle)
    {
        if (!$this->produitManager->isDateAvailable($dateArrivee, $dateDepart,$idSalle)) {
            $this->errDateArrivee = 'Ces dates ne sont pas disponible pour cette salle';
            $this->errDateDepart = 'Ces dates ne sont pas disponible pour cette salle';
            return $this->error++;
        }
        
        if ($dateDepart < $dateArrivee) {
            $this->errDateArrivee = 'La date d\'arrivée doit être antérieure à la date de départ.';
            $this->errDateDepart = 'La date de départ doit être postérieure à la date d\'arrivée.';
            return $this->error++;
        }
    }
    
    private function checkPrix($prix)
    {
        if ($this->constraint->isNotEmpty($prix)) {
            $this->errPrix = $this->constraint->isNotEmpty($prix);
            return $this->error++;
        }
        
        if (!is_numeric($prix)) {
            $this->errPrix = 'Le prix doit être un chiffre';
            return $this->error++;
        }
    }
    
    private function checkSalle($salle)
    {
        if ($this->constraint->isNotEmpty($salle)) {
            $this->errSalle = $this->constraint->isNotEmpty($salle);
            return $this->error++;
        }
    }
}