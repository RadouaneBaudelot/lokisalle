<?php

class PromoHandler
{
    protected $error;
    protected $constraint;
    protected $promo;
    protected $promoManager;
    protected $errCodePromo;
    protected $errReduction;
    
    public function __construct()
    {
        $this->constraint = new Constraint();
        $this->promo = new Promo();
        $this->promoManager = new PromoManager();
    }
    
    public function checkDatasForCreation()
    {
        // Intialize error property
        $this->error = 0;
        
        // check form validity
        $this->checkCodePromoForCreation($this->promo->getCodePromo());
        $this->checkReductionForCreation($this->promo->getReduction());
        
        // Check if errors has been returned during checks
        if ($this->error == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function getErrCodePromo() {
        return $this->errCodePromo;
    }

    public function getErrReduction() {
        return $this->errReduction;
    }

    private function checkCodePromoForCreation($codePromo)
    {
        if ($this->constraint->isNotEmpty($codePromo)) {
            $this->errCodePromo = $this->constraint->isNotEmpty($codePromo);
            return $this->error++;
        }
        
        if ($this->constraint->minLength($codePromo, 5)){
            $this->errCodePromo .= $this->constraint->minLength($codePromo, 6);
            $this->error++;
        }
        
        if ($this->constraint->maxLength($codePromo, 7)){
            $this->errCodePromo .= $this->constraint->maxLength($codePromo, 6);
            $this->error++;
        }
        return $this->error;
    }
    
    private function checkReductionForCreation($reduction)
    {
        if ($this->constraint->isNotEmpty($reduction)) {
            $this->errReduction = $this->constraint->isNotEmpty($reduction);
            return $this->error++;
        }
        
        if(!is_numeric($reduction)) {
            $this->errReduction = 'La réduction doit être un chiffre';
            return $this->error++;
        }
    }
}
