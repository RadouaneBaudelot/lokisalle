<?php

/**
 * 2015
 * 
 * @author Radouane Baudelot <r.baudelot@gmail.com>
 * 
 * @package Membre
 * 
 * Service that handle Membre Entity
 */


class MembreHandler
{
    // Properties declaration     
    protected $constraint;
    protected $membre;
    protected $membreManager;
    protected $error;
    protected $errPseudo;
    protected $errMdp;
    protected $errCheckMdp;
    protected $errNom;
    protected $errPrenom;
    protected $errEmail;
    protected $errCheckEmail;
    protected $errSexe;
    protected $errVille;
    protected $errCp;
    protected $errAdresse;
    
    /**
     * Instanciation of objects used in this class
     */
    public function __construct()
    {
        $this->membre = new Membre();
        $this->constraint = new Constraint();
        $this->membreManager = new MembreManager();
    }
    
    /**
     * Check All Datas sent in registration Form
     * 
     * @return boolean
     * TRUE if no errors
     * FALSE if errors
     */
    public function checkDatasForRegistration($checkMdp, $checkEmail)
    {
        // Intialize error property
        $this->error = 0;
        
        // check form validity
        $this->checkPseudoForRegistration($this->membre->getPseudo());
        $this->checkMdpForRegistration($this->membre->getMdp());
        $this->checkMdpCheckForRegistration($checkMdp, $this->membre->getMdp());
        $this->checkNameForRegistration($this->membre->getNom());
        $this->checkFirstNameForRegistration($this->membre->getPrenom());
        $this->checkEmailForRegistration($this->membre->getEmail());
        $this->checkEmailCheckForRegistration($checkEmail, $this->membre->getEmail());
        $this->checkSexeForRegistration($this->membre->getSexe());
        $this->checkVilleForRegistration($this->membre->getVille());
        $this->checkCpForRegistration($this->membre->getCp());
        $this->checkAdresseForRegistration($this->membre->getAdresse());
        
        // Check if errors has been returned during checks
        if ($this->error == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function checkDatasForUpdate($oldPseudo, $newPseudo, $checkMdp, $oldEmail, $newEmail, $checkEmail)
    {
        // Intialize error property
        $this->error = 0;
        
        // check form validity
        $this->checkPseudoForUpdate($oldPseudo, $newPseudo);
        $this->checkMdpForRegistration($this->membre->getMdp());
        $this->checkMdpCheckForRegistration($checkMdp, $this->membre->getMdp());
        $this->checkNameForRegistration($this->membre->getNom());
        $this->checkFirstNameForRegistration($this->membre->getPrenom());
        $this->checkEmailForUpdate($oldEmail, $newEmail);
        $this->checkEmailCheckForRegistration($checkEmail, $this->membre->getEmail());
        $this->checkSexeForRegistration($this->membre->getSexe());
        $this->checkVilleForRegistration($this->membre->getVille());
        $this->checkCpForRegistration($this->membre->getCp());
        $this->checkAdresseForRegistration($this->membre->getAdresse());
        
        // Check if errors has been returned during checks
        if ($this->error == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function checkDatasForConnection()
    {
        $this->error = 0;
        
        $this->checkPseudoForConnection($this->membre->getPseudo());
        $this->checkMdpForConnection($this->membre->getMdp(), $this->membre->getPseudo());
                
        if ($this->error == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function connect($pseudo)
    {
        $membre = $this->membreManager->getMembreByPseudo($pseudo);
        if ($membre->getStatut() == 2) { // check if member is an Admin
            $_SESSION['member']['status'] = 2; // if admin, status = 2
        } else {
            $_SESSION['member']['status'] = 1; // else status = 1
        }
        $_SESSION['member']['idMembre'] = $membre->getIdMembre();
        $_SESSION['member']['pseudo'] = $membre->getPseudo();
        $_SESSION['member']['email'] = $membre->getEmail();
    }
    
    public function checkInscriptionNewsletter($id)
    {
        if ($this->membreManager->membreAlreadyRegisteredToNl($id)) {
            return FALSE;
        }
        
        return TRUE;
    }
    
    //GETTERS for Error Messages
    public function getErrPseudo() {
        return $this->errPseudo;
    }
    
    public function getErrMdp() {
        return $this->errMdp;
    }
    
    public function getErrCheckMdp() {
        return $this->errCheckMdp;
    }

    public function getErrNom() {
        return $this->errNom;
    }

    public function getErrPrenom() {
        return $this->errPrenom;
    }
    
    public function getErrEmail() {
        return $this->errEmail;
    }
    
    public function getErrCheckEmail() {
        return $this->errCheckEmail;
    }
    
    public function getErrSexe() {
        return $this->errSexe;
    }
    
    public function getErrVille() {
        return $this->errVille;
    }
    
    public function getErrCp() {
        return $this->errCp;
    }
    
    public function getErrAdresse() {
        return $this->errAdresse;
    }

    //METHODS THAT CHECK REGISTRATION FORM
    private function checkPseudoForRegistration($pseudo)
    {
        if ($this->constraint->isNotEmpty($pseudo)) {
            $this->errPseudo = $this->constraint->isNotEmpty($pseudo);
            return $this->error++;
        }
        
        if ($this->constraint->minLength($pseudo, 2)){
            $this->errPseudo .= $this->constraint->minLength($pseudo, 2);
            $this->error++;
        }
        
        if ($this->constraint->maxLength($pseudo, 45)){
            $this->errPseudo .= $this->constraint->maxLength($pseudo, 45);
            $this->error++;
        }
        
        if ($this->membreManager->pseudoExists($pseudo)) {
            $this->errPseudo .= 'Ce pseudo existe déjà, merci d\'en choisir un autre';
            $this->error++;
        }
        return $this->error;
    }
    
    private function checkMdpForRegistration($mdp)
    {
        if ($this->constraint->isNotEmpty($mdp)) {
            $this->errMdp = $this->constraint->isNotEmpty($mdp);
            return $this->error++;
        }
        
        if ($this->constraint->minLength($mdp, 2)){
            $this->errMdp = $this->constraint->minLength($mdp, 2);
            return $this->error++;
        }
        
        if ($this->constraint->maxLength($mdp, 45)){
            $this->errMdp = $this->constraint->maxLength($mdp, 45);
            return $this->error++;
        }
    }
    
    private function checkMdpCheckForRegistration($checkMdp, $mdp)
    {
        if ($this->constraint->isNotEmpty($checkMdp) && $this->constraint->isNotEmpty($mdp)) {
            $this->errCheckMdp = $this->constraint->isNotEmpty($checkMdp);
            return $this->error++;
        }
        
        if($mdp !== $checkMdp) {
            $this->errCheckMdp = 'Les 2 mots de passes ne sont pas identiques, merci de bien vouloir recommencer';
            return $this->error++;
        }
    }
    
    private function checkNameForRegistration($name)
    {
        if ($this->constraint->isNotEmpty($name)) {
            $this->errNom = $this->constraint->isNotEmpty($name);
            return $this->error++;
        }
        
        if ($this->constraint->maxLength($name, 45)){
            $this->errNom = $this->constraint->maxLength($name, 45);
            return $this->error++;
        }
    }
    
    private function checkFirstNameForRegistration($firstName)
    {
        if ($this->constraint->isNotEmpty($firstName)) {
            $this->errPrenom = $this->constraint->isNotEmpty($firstName);
            return $this->error++;
        }
        
        if ($this->constraint->maxLength($firstName, 45)){
            $this->errPrenom = $this->constraint->maxLength($firstName, 45);
            return $this->error++;
        }
    }
    
    private function checkEmailForRegistration($email)
    {
        if ($this->constraint->isNotEmpty($email)) {
            $this->errEmail = $this->constraint->isNotEmpty($email);
            return $this->error++;
        }
        
        if ($this->constraint->isEmailCorrect($email)) {
            $this->errEmail = $this->constraint->isEmailCorrect($email);
            return $this->error++;
        }
        
        if ($this->constraint->emailExists($email)) {
            $this->errEmail = 'Cette adresse email existe déjà.';
            return $this->error++;
        }
    }
    
    private function checkEmailCheckForRegistration($checkEmail, $email)
    {
        if ($this->constraint->isNotEmpty($checkEmail) && $this->constraint->isNotEmpty($email)) {
            $this->errCheckEmail = $this->constraint->isNotEmpty($checkEmail);
            return $this->error++;
        }
        
        if($email !== $checkEmail) {
            $this->errCheckEmail = 'Les 2 emails ne sont pas identiques, merci de bien vouloir recommencer';
            return $this->error++;
        }
    }

    private function checkSexeForRegistration($sexe)
    {
        if (!$this->constraint->isNotEmpty($sexe)) {
            if ($this->constraint->isValidSexe($sexe)) {
            $this->errSexe = $this->constraint->isValidSexe($sexe);
            $this->error++;
            }
        } else {
            $this->errSexe = $this->constraint->isNotEmpty($sexe);
            $this->error++;
        }
        return $this->error;
    }
    
    private function checkVilleForRegistration($ville)
    {
        if ($this->constraint->isNotEmpty($ville)) {
            $this->errVille = $this->constraint->isNotEmpty($ville);
            return $this->error++;
        }
        
        if ($this->constraint->maxLength($ville, 45)) {
            $this->errVille = $this->constraint->maxLength($ville, 45);
            return $this->error++;
        }
    }

    private function checkCpForRegistration($cp)
    {
        if (!$this->constraint->isNotEmpty($cp)) {
            if ($this->constraint->isValidCp($cp)) {
            $this->errCp = $this->constraint->isValidCp($cp);
            $this->error++;
            }
        } else {
            $this->errCp = $this->constraint->isNotEmpty($cp);
            $this->error++;
        }
        return $this->error;
    }
    
    private function checkAdresseForRegistration($adresse)
    {
        if ($this->constraint->isNotEmpty($adresse)) {
            $this->errAdresse = $this->constraint->isNotEmpty($adresse);
            return $this->error++;
        }
        
        if ($this->constraint->maxLength($adresse, 120)) {
            $this->errAdresse = $this->constraint->maxLength($adresse, 120);
            return $this->error++;
        }
    }
    
    private function checkPseudoForConnection($pseudo)
    {
        if ($this->constraint->isNotEmpty($pseudo)) {
            $this->errPseudo = $this->constraint->isNotEmpty($pseudo);
            return $this->error++;
        }
        
        if (!$this->constraint->pseudoExists($pseudo)) {
            $this->errPseudo = 'Ce pseudo n\'existe pas';
            return $this->error++;
        }
    }
    
    private function checkMdpForConnection($mdp, $pseudo)
    {
        if ($this->constraint->isNotEmpty($mdp) && $this->constraint->isNotEmpty($pseudo)) {
            $this->errMdp = $this->constraint->isNotEmpty($mdp);
            return $this->error++;
        }
        
        if ($mdp !== $this->membreManager->getMdpFromPseudo($pseudo)) {
            $this->errMdp = 'Le mot de passe est incorrect.';
            return $this->error++;
        }
    }
    
    private function checkPseudoForUpdate($oldPseudo, $newPseudo)
    {
        if ($newPseudo !== $oldPseudo) {
            $this->checkPseudoForRegistration($newPseudo);
        }
    }
    
    private function checkEmailForUpdate($oldEmail, $newEmail)
    {
        if ($newEmail !== $oldEmail) {
            $this->checkEmailForRegistration($newEmail);
        }
    }
}