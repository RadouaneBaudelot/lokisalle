<?php

class AvisHandler
{
    protected $constraint;
    protected $avis;
    protected $error;
    protected $errCommentaire;
    protected $errNote;
    
    public function __construct()
    {
        $this->avis = new Avis();
        $this->constraint = new Constraint();
    }
    
    public function checkDatas()
    {
        // Intialize error property
        $this->error = 0;
        
        // check form validity
        $this->checkCommentaire($this->avis->getCommentaire());
        $this->checkNote($this->avis->getNote());
        
        // Check if errors has been returned during checks
        if ($this->error == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function getErrCommentaire() {
        return $this->errCommentaire;
    }

    public function getErrNote() {
        return $this->errNote;
    }

    private function checkCommentaire($commentaire)
    {
        if ($this->constraint->isNotEmpty($commentaire)) {
            $this->errCommentaire = $this->constraint->isNotEmpty($commentaire);
            return $this->error++;
        }
        
        if ($this->constraint->minLength($commentaire, 2)){
            $this->errCommentaire .= $this->constraint->minLength($commentaire, 2);
            $this->error++;
        }
        
        if ($this->constraint->maxLength($commentaire, 500)){
            $this->errCommentaire .= $this->constraint->maxLength($commentaire, 500);
            $this->error++;
        }
    }

    private function checkNote($note)
    {
        if ($this->constraint->isNotEmpty($note)) {
            $this->errNote = $this->constraint->isNotEmpty($note);
            return $this->error++;
        }
        
        if(!is_numeric($note)) {
            $this->errNote = 'La note doit être un chiffre';
            return $this->error++;
        }
        
        if($note > 10 || $note < 0) {
            $this->errNote = 'La note doit être comprise entre 0 et 10';
            return $this->error++;
        }
    }
}