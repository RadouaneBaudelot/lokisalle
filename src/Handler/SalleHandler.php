<?php

class SalleHandler
{
    protected $constraint;
    protected $salle;
    protected $salleManager;
    protected $error;
    protected $errPays;
    protected $errVille;
    protected $errAdresse;
    protected $errCp;
    protected $errTitre;
    protected $errDescription;
    protected $errPhoto;
    protected $errCapacite;
    protected $errCategorie;
    
    public function __construct()
    {
        $this->constraint = new Constraint();
        $this->salle = new Salle();
        $this->salleManager = new SalleManager();
    }
    
    public function checkDatasForCreation()
    {
        // Intialize error property
        $this->error = 0;
        
        // check form validity
        $this->checkPaysForCreation($this->salle->getPays());
        $this->checkVilleForCreation($this->salle->getVille());
        $this->checkAdresseForCreation($this->salle->getAdresse());
        $this->checkCpForCreation($this->salle->getCp());
        $this->checkTitreForCreation($this->salle->getTitre());
        $this->checkDescriptionForCreation($this->salle->getDescription());
        $this->checkPhotoForCreation('photo');
        $this->checkCapaciteForCreation($this->salle->getCapacite());
        $this->checkCategorieForCreation($this->salle->getCategorie());
        
        // Check if errors has been returned during checks
        if ($this->error == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    //GETTERS for Error Messages
    public function getErrPays() {
        return $this->errPays;
    }

    public function getErrVille() {
        return $this->errVille;
    }

    public function getErrAdresse() {
        return $this->errAdresse;
    }

    public function getErrCp() {
        return $this->errCp;
    }

    public function getErrTitre() {
        return $this->errTitre;
    }

    public function getErrDescription() {
        return $this->errDescription;
    }

    public function getErrPhoto() {
        return $this->errPhoto;
    }

    public function getErrCapacite() {
        return $this->errCapacite;
    }

    public function getErrCategorie() {
        return $this->errCategorie;
    }

    private function checkPaysForCreation($pays)
    {
        if ($this->constraint->isNotEmpty($pays)) {
            $this->errPays = $this->constraint->isNotEmpty($pays);
            return $this->error++;
        }
        
        if ($this->constraint->minLength($pays, 2)){
            $this->errPays .= $this->constraint->minLength($pays, 2);
            $this->error++;
        }
        
        if ($this->constraint->maxLength($pays, 45)){
            $this->errPays .= $this->constraint->maxLength($pays, 45);
            $this->error++;
        }
    }
    
    private function checkVilleForCreation($ville)
    {
        if ($this->constraint->isNotEmpty($ville)) {
            $this->errVille = $this->constraint->isNotEmpty($ville);
            return $this->error++;
        }
        
        if ($this->constraint->minLength($ville, 2)){
            $this->errVille .= $this->constraint->minLength($ville, 2);
            $this->error++;
        }
        
        if ($this->constraint->maxLength($ville, 45)) {
            $this->errVille = $this->constraint->maxLength($ville, 45);
            return $this->error++;
        }
    }
    
    private function checkAdresseForCreation($adresse)
    {
        if ($this->constraint->isNotEmpty($adresse)) {
            $this->errAdresse = $this->constraint->isNotEmpty($adresse);
            return $this->error++;
        }
        
        if ($this->constraint->maxLength($adresse, 120)) {
            $this->errAdresse = $this->constraint->maxLength($adresse, 120);
            return $this->error++;
        }
    }

    private function checkCpForCreation($cp)
    {
        if (!$this->constraint->isNotEmpty($cp)) {
            if ($this->constraint->isValidCp($cp)) {
            $this->errCp = $this->constraint->isValidCp($cp);
            $this->error++;
            }
        } else {
            $this->errCp = $this->constraint->isNotEmpty($cp);
            $this->error++;
        }
        return $this->error;
    }
    
    private function checkTitreForCreation($titre)
    {
        if ($this->constraint->isNotEmpty($titre)) {
            $this->errTitre = $this->constraint->isNotEmpty($titre);
            return $this->error++;
        }
        
        if ($this->constraint->minLength($titre, 2)){
            $this->errTitre .= $this->constraint->minLength($titre, 2);
            $this->error++;
        }
        
        if ($this->constraint->maxLength($titre, 45)){
            $this->errTitre .= $this->constraint->maxLength($titre, 45);
            $this->error++;
        }
    }
    
    private function checkDescriptionForCreation($description)
    {
        if ($this->constraint->isNotEmpty($description)) {
            $this->errDescription = $this->constraint->isNotEmpty($description);
            return $this->error++;
        }
    }
    
    private function checkPhotoForCreation($photo)
    {
        $upload = new Upload();
        
        if(!$upload->checkExtension($photo)) {
            $this->errPhoto = 'La photo doit être au format gif, jpg, jpeg ou png';
            return $this->error++;
        }
    }
    
    private function checkCapaciteForCreation($capacite)
    {
        if ($this->constraint->isNotEmpty($capacite)) {
            $this->errCapacite = $this->constraint->isNotEmpty($capacite);
            return $this->error++;
        }
        
        if(!is_numeric($capacite)) {
            $this->errCapacite = 'La capacité doit être un chiffre';
            return $this->error++;
        }
    }
    
    private function checkCategorieForCreation($categorie)
    {
        if (!$this->constraint->isNotEmpty($categorie)) {
            if ($this->constraint->isValidCategorie($categorie)) {
            $this->errCategorie = $this->constraint->isValidCategorie($categorie);
            $this->error++;
            }
        } else {
            $this->errCategorie = $this->constraint->isNotEmpty($categorie);
            $this->error++;
        }
        return $this->error;
    }
}
