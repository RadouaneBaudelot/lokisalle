<div id="ajouterSalle">
    <h1>Ajouter une salle</h1>
    <form action="" method="post" name="salle" enctype="multipart/form-data">
        <label for="pays">Pays</label>
        <input type="text" id="pays" name="pays" value="<?php echo $salle->getPays(); ?>" />
        <span class="error"><?php echo $error->getErrPays(); ?></span><br />
        
        <label for="ville">Ville</label>
        <input type="text" id="ville" name="ville" value="<?php echo $salle->getVille(); ?>" />
        <span class="error"><?php echo $error->getErrVille(); ?></span><br />
        
        <label for="adresse">Adresse</label>
        <input type="text" id="adresse" name="adresse" value="<?php echo $salle->getAdresse(); ?>" />
        <span class="error"><?php echo $error->getErrAdresse(); ?></span><br />
        
        <label for="cp">Code Postal</label>
        <input type="text" id="cp" name="cp" value="<?php echo $salle->getCp(); ?>" />
        <span class="error"><?php echo $error->getErrCp(); ?></span><br />
        
        <label for="titre">Titre</label>
        <input type="text" id="titre" name="titre" value="<?php echo $salle->getTitre(); ?>" />
        <span class="error"><?php echo $error->getErrTitre(); ?></span><br />
        
        <label for="description">Description</label>
        <textarea id="description" name="description" value="<?php echo $salle->getDescription() ?>" ></textarea>
        <span class="error"><?php echo $error->getErrDescription(); ?></span><br />
        
        <label for="photo">Photo</label>
        <input type="file" id="photo" name="photo" />
        <span class="error"><?php echo $error->getErrPhoto(); ?></span><br />
        
        <label for="capacite">Capacite</label>
        <input type="text" id="capacite" name="capacite" value="<?php echo $salle->getCapacite(); ?>" />
        <span class="error"><?php echo $error->getErrCapacite(); ?></span><br />
        
        <label for="categorie">Categorie</label>
        <input type="radio" name="categorie" value="cat1" checked="checked">Cat1
        <input type="radio" name="categorie" value="cat2" class='radio'>Cat2
        <span class="error"><?php echo $error->getErrCategorie(); ?></span><br />
        
        <input type="submit" name="ajouter" value="Ajouter une salle"><br />
    </form>
</div>

