<div id="promo">
    <h1>Ajouter un code promo</h1>
    <form action="" method="post" name="promo">
        <label for="codePromo">Code Promo</label>
        <input type="text" id="codePromo" name="codePromo" value="<?php echo $promo->getCodePromo(); ?>" />
        <span class="error"><?php echo $error->getErrCodePromo(); ?></span><br />
        
        <label for="reduction">Réduction</label>
        <input type="text" id="reduction" name="reduction" value="<?php echo $promo->getReduction(); ?>" />
        <span class="error"><?php echo $error->getErrReduction(); ?></span><br />
        
        <input type="submit" name="promo" value="Créer">
    </form>
</div>