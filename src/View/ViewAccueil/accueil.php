<div id='presentation'>
    <h1>Bienvenue sur LokiSalle</h1>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit non augue id placerat. Phasellus nibh metus, laoreet vel lectus laoreet, dignissim vestibulum sem. Nunc lobortis nulla in tortor vestibulum, vulputate dignissim orci convallis. Donec consequat nibh erat, a condimentum nulla porta varius. Nam tristique eu augue pulvinar facilisis. Suspendisse dictum augue finibus mi ultrices lobortis. Quisque vitae nunc porta, tempor nisi ut, vulputate est. Integer efficitur tellus lorem, ac porta magna consectetur id. Vestibulum elementum, neque a iaculis luctus, justo dolor tristique metus, at imperdiet libero nulla et orci. Donec egestas, sem sit amet blandit posuere, lorem nisl tincidunt lacus, vel ullamcorper sem metus vel justo.
    </p>   
    <p>
        Sed pulvinar ligula nulla, id rhoncus purus tristique aliquet. Duis tempor tincidunt erat. Curabitur lectus nunc, facilisis sit amet felis dictum, dignissim congue nunc. Suspendisse nisl libero, finibus in enim et, lacinia tristique enim. Nunc pulvinar, risus in consectetur pharetra, velit lectus pellentesque leo, ac sodales metus nulla et nisl. Nunc mollis, velit eget fermentum ultricies, diam libero fringilla quam, vel imperdiet ipsum felis ac urna. Praesent vel diam ullamcorper odio viverra ullamcorper ut ut nulla. Proin eu diam est.
    </p>
    <p>
        Sed pulvinar ligula nulla, id rhoncus purus tristique aliquet. Duis tempor tincidunt erat. Curabitur lectus nunc, facilisis sit amet felis dictum, dignissim congue nunc. Suspendisse nisl libero, finibus in enim et, lacinia tristique enim. Nunc pulvinar, risus in consectetur pharetra, velit lectus pellentesque leo, ac sodales metus nulla et nisl. Nunc mollis, velit eget fermentum ultricies, diam libero fringilla quam, vel imperdiet ipsum felis ac urna. Praesent vel diam ullamcorper odio viverra ullamcorper ut ut nulla. Proin eu diam est.
    </p>
</div>
<div id='dernieres_offres'>
    <h1>Nos 3 dernières offres</h1>
        <?php
        foreach ($produits as $produit) {
            echo '<div class="mea">';
            echo '<img src="'.$produit->getSalle()->getPhoto().'" />';
            echo '<h3>' . $produit->getSalle()->getTitre() . '</h3><br />';
            echo '<span>' . $produit->getSalle()->getVille() . ', ' . $produit->getSalle()->getPays() . '</span><br />';
            echo '<span>Du ' . $produit->getArrivee()->format('d/m/Y') . ' au ' . $produit->getArrivee()->format('d/m/Y') . '</span><br />';
            echo '<span>' . $produit->getPrix() . ' euros pour ' . $produit->getSalle()->getCapacite() . ' personnes</span><br />';
            echo '<a href="'.RACINE_SITE.'/produit/detail/'.$produit->getIdProduit().'">Voir la fiche détaillée</a><br />';
            echo '<br />';
            if (isset($_SESSION['panier'][$produit->getIdProduit()])) { 
                echo '<p style="margin-left: 120px;">Produit ajouté au panier</p>';
            } elseif ($_SESSION['member']['status'] === 1 || $_SESSION['member']['status'] === 2) {
                ?>
                <div class="cart">    
                    <form action="" method="post" name="panier">
                        <input type="submit" name="ajouter<?php echo $produit->getIdProduit(); ?>" value="Ajouter au panier" /><br />
                    </form>
                </div>
                <?php
            } else {
                echo '<a href="'.RACINE_SITE.'/membre/connexion">Connectez-vous pour l\'ajouter au panier</a><br />';
            }
            echo '</div>';
        }
        ?>
</div>