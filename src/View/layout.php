<!DOCTYPE html>
<html lang="fr">
    <head>
	<meta charset="utf-8">
        <meta name="robots" content="noindex">
        <title>LokiSalle - <?php echo $title .'/'. $action; ?></title>
        <link rel="icon" type="image/png" href="" />
        <link rel="stylesheet" type="text/css" href="<?php echo RACINE_SITE; ?>/css/style.css">
        <script type="text/javascript" src="<?php echo RACINE_SITE; ?>/js/globalJS.js"></script>
    </head>
    <body>
        <div id="connexion">
            <div class="connexion">
                <?php if ($_SESSION['member']['status'] === 0) { ?>
                <ul>
                    <li><a href="<?php echo RACINE_SITE; ?>/membre/connexion">Connexion</a></li>
                    <li><p>|</p></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/membre/inscription">Inscription</a></li>
                </ul>
                <?php } ?>
                <?php if ($_SESSION['member']['status'] === 1
                        || $_SESSION['member']['status'] === 2) { ?>
                <ul>
                    <li><a href="<?php echo RACINE_SITE; ?>/membre/profil">Profil</a></li>
                    <li><p>|</p></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/panier/afficher">Panier</a></li>
                    <li><p>|</p></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/membre/deconnexion">Deconnexion</a></li>
                </ul>
                <?php } ?>
            </div>
        </div>
        <div id="header">
            <div class="logo">
                <a href="<?php echo RACINE_SITE; ?>/accueil/afficher">
                    <img src="<?php echo RACINE_SITE; ?>/img/loki-urban.png" width="220" alt="logo" />
                </a>
            </div>
            <nav>
                <ul>
                    <li><a href="<?php echo RACINE_SITE; ?>">Accueil</a></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/produit/reservation">Réservation</a></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/produit/recherche">Recherche</a></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/membre/contact">Contact</a></li>
                </ul>
            </nav> 
        </div>
        <?php if ($_SESSION['member']['status'] === 2) { ?>
        <div id="adminmenu">
            <div class="adminmenu">
                <ul>
                    <li><a href="<?php echo RACINE_SITE; ?>/salle/gestion">Salles</a></li>
                    <li><p>|</p></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/produit/gestion">Produits</a></li>
                    <li><p>|</p></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/membre/gestion">Membres</a></li>
                    <li><p>|</p></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/commande/gestion">Commandes</a></li>
                    <li><p>|</p></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/avis/gestion">Avis</a></li>
                    <li><p>|</p></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/promo/gestion">Promos</a></li>
                    <li><p>|</p></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/statistique/ajouter">Statistiques</a></li>
                    <li><p>|</p></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/membre/envoyernl">Newsletters</a></li>
                </ul>
            </div>
        </div>
        <?php } ?>
        <div id="contenu">
            <?php echo $buffer; ?>
        </div>
        <div id="footer">
            <nav>
                <ul>
                    <li><a href="">Mentions légales</a></li>
                    <li><p>|</p></li>
                    <li><a href="">C.G.V</a></li>
                    <li><p>|</p></li>
                    <li><a href="">Plan du Site</a></li>
                    <li><p>|</p></li>
                    <li><a href="" onclick="imprimer()">Imprimer la page</a></li>
                    <li><p>|</p></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/membre/newsletter">S'inscrire à la newsletter</a></li>
                    <li><p>|</p></li>
                    <li><a href="<?php echo RACINE_SITE; ?>/membre/contact">Contact</a></li>
                </ul>
            </nav>
            <div class="logo_footer">
                <img src="<?php echo RACINE_SITE; ?>/img/loki-urban.png" width="100" alt="logo" />
            </div>
        </div>
    </body>
</html>