<div id="gestion">
    <h1>Avis</h1>
    <table>
        <th>Id</th>
        <th>Commentaire</th>
        <th>Note</th>
        <th>Salle</th>
        <th>Membre</th>
        <th>Supprimer</th>
    <?php 
    foreach ($commentaires as $commentaire) {
            echo '<tr>';    
            echo '<td>' . $commentaire->getIdAvis() . '</td>';
            echo '<td>' . $commentaire->getCommentaire() . '</td>';
            echo '<td>' . $commentaire->getNote() . '</td>';
            echo '<td>' . $avisManager->findById('salle', $commentaire->getIdSalle())->getTitre() . '</td>';
            echo '<td>' . $commentaire->getMembre()->getPseudo() . '</td>';
            echo '<td><a href="'.RACINE_SITE.'/avis/supprimer/'.$commentaire->getIdAvis().'">X</a></td>';
            echo '</tr>';
        }
    ?>
    </table>
</div>