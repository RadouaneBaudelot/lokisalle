<div id="envoyernl">
    <h1>Envoyer une newsletter</h1>
    
    <p>Nombre d'abonnés à la newsletter : <?php echo $abonnes ?></p>
    
    <form action="" method="post" name="envoyernl">
        <label for="sujet">Sujet</label>
        <input type="text" id="sujet" name="sujet" value="<?php if(isset($_POST['sujet'])) { echo $_POST['sujet']; } ?>" /><br />
                
        <label for="message">Message</label>
        <textarea id="message" name="message" value="<?php if(isset($_POST['message'])) { echo $_POST['message']; } ?>" ></textarea><br />
        
        <input type="hidden" id="sender" name="sender" value="contact@lokisalle.com" /><br />
        
        <input type="submit" name="envoyernl" value="Envoyer">
    </form>
</div>