<div id="contact">
    <h1>Contact</h1>
    <form action="" method="post" name="contact">
        <label for="sujet">Sujet</label>
        <input type="text" id="sujet" name="sujet" value="<?php if(isset($_POST['sujet'])) { echo $_POST['sujet']; } ?>" /><br />
        
        <label for="sender">Expéditeur</label>
        <input type="text" id="sender" name="sender" value="<?php if(isset($_SESSION['member']['email'])) { echo $_SESSION['member']['email']; } ?>" /><br />
        
        <label for="name">Nom ou pseudo</label>
        <input type="text" id="name" name="name" value="<?php if(isset($_SESSION['member']['pseudo'])) { echo $_SESSION['member']['pseudo']; } ?>" /><br />
        
        <label for="message">Message</label>
        <textarea id="message" name="message" value="<?php if(isset($_POST['message'])) { echo $_POST['message']; } ?>" ></textarea><br />
        
        <input type="hidden" id="email" name="email" value="r.baudelot@gmail.com" /><br />
        
        <input type="submit" name="contact" value="Envoyer">
    </form>
</div>