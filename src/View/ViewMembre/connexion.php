<div id='connect'>
    <div class="bloc-gauche">
        <h1>Connexion</h1>
        <form action="" method="post" name="connexion">
            <label for="pseudo">Pseudo</label>
            <input type="text" id="pseudo" name="pseudo" value="<?php echo $membre->getPseudo(); ?>" />
            <span class="error"><?php echo $error->getErrPseudo(); ?></span><br />

            <label for="mdp">Mot de Passe</label>
            <input type="password" id="mdp" name="mdp" />
            <span class="error"><?php echo $error->getErrMdp(); ?></span><br />
            
            <input type="submit" name="connexion" value="Connexion">
        </form>
        <span>
            <a href="<?php echo RACINE_SITE; ?>/membre/pass">Mot de passe oublié ?</a>
        </span>
    </div>
    <div class ="bloc-droite">
        <h1>S'inscrire</h1>
        <p>Pas encore de compte ?</p>
        <p><a href="<?php echo RACINE_SITE; ?>/membre/inscription">Inscrivez-vous</a></p>
    </div>
</div>