<div id='inscription'>
    <h1>Modification</h1>
    <form action="" method="post" name="inscription">
        <label for="pseudo">Pseudo</label>
        <input type="text" id="pseudo" name="pseudo" value="<?php echo $membre->getPseudo(); ?>" />
        <span class="error"><?php echo $error->getErrPseudo(); ?></span><br />
        
        <label for="mdp">Mot de Passe</label>
        <input type="password" id="mdp" name="mdp" />
        <span class="error"><?php echo $error->getErrMdp(); ?></span><br />
        
        <label for="checkMdp">Retapez Mot de Passe</label>
        <input type="password" id="checkMdp" name="checkMdp" />
        <span class="error"><?php echo $error->getErrCheckMdp(); ?></span><br />
       
        <label for="nom">Nom</label>
        <input type="text" id="nom" name="nom" value="<?php echo $membre->getNom(); ?>" />
        <span class="error"><?php echo $error->getErrNom(); ?></span><br />

        <label for="prenom">Prénom</label>
        <input type="text" id="prenom" name="prenom" value="<?php echo $membre->getPrenom(); ?>" />
        <span class="error"><?php echo $error->getErrPrenom(); ?></span><br />
        
        <label for="email">Email</label>
        <input type="email" id="email" name="email" value="<?php echo $membre->getEmail(); ?>" />
        <span class="error"><?php echo $error->getErrEmail(); ?></span><br />
        
        <label for="checkEmail">Retapez Email</label>
        <input type="email" id="checkEmail" name="checkEmail" />
        <span class="error"><?php echo $error->getErrCheckEmail(); ?></span><br />
        
        <label for="sexe">Sexe</label>
        <input type="radio" name="sexe" value="m" checked="checked">Homme
        <input type="radio" name="sexe" value="f" class='radio'>Femme
        <span class="error"><?php echo $error->getErrSexe(); ?></span><br />
        
        <label for="ville">Ville</label>
        <input type="text" id="ville" name="ville" value="<?php echo $membre->getVille(); ?>" />
        <span class="error"><?php echo $error->getErrVille(); ?></span><br />
        
        <label for="cp">Code Postal</label>
        <input type="text" id="cp" name="cp" value="<?php echo $membre->getCp(); ?>" />
        <span class="error"><?php echo $error->getErrCp(); ?></span><br />
        
        <label for="adresse">Adresse</label>
        <input type="text" id="adresse" name="adresse" value="<?php echo $membre->getAdresse(); ?>" />
        <span class="error"><?php echo $error->getErrAdresse(); ?></span><br />
        
        <input type="submit" name="modification" value="Mettre à jour">
        
    </form>
</div>