<div id="profil">
    <h1>Bonjour <?php echo $membre->getPseudo(); ?></h1>
    <p>Pseudo : <?php echo $membre->getPseudo(); ?></p>
    <p>Nom : <?php echo $membre->getNom(); ?></p>
    <p>Prénom : <?php echo $membre->getPrenom(); ?></p>
    <p>Email : <?php echo $membre->getEmail(); ?></p>
    <p>Sexe : <?php echo $membre->getSexe() == 'm' ? 'Homme' : 'Femme'; ?></p>
    <p>Adresse : <?php echo $membre->getAdresse(); ?></p>
    <p>Cp : <?php echo $membre->getCp(); ?></p>
    <p>Ville : <?php echo $membre->getVille(); ?></p>
    <br />
    <a href="<?php echo RACINE_SITE; ?>/membre/modification">Mettre à jour mes informations</a>
</div>
