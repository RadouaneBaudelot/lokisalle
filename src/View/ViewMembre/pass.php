<div id="lostpass">
    <h1>Mot de passe oublié</h1
    
    <span>
        <p>Veuillez saisir l'adresse email associée à votre compte. Un email vous sera envoyé avec un nouveau mot de passe.</p>
    </span>
    
    <form action="" method="post" name="pass">
        <label for="email">Email</label>
        <input type="email" id="email" name="email" value="<?php echo $membre->getEmail(); ?>" />
        <span class="error"><?php echo $error->getErrEmail(); ?></span><br />
        
        <input type="submit" name="pass" value="Valider">
    </form>
</div>
