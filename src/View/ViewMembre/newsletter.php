<div id="newsletter">
    <h1>Newsletter</h1>
    <?php if (isset($_SESSION['member']['idMembre'])) { ?>
    <form method="post" name="newsletter">
        <input type="submit" name="newsletter" value="S'inscrire à la newsletter" id='newsletterlink' onmouseover="this.style.cursor='pointer'">
    </form>
    <?php } else { ?>
    <span>
        <a href="<?php echo RACINE_SITE; ?>/membre/connexion">Connectez-vous</a>
        ou
        <a href="<?php echo RACINE_SITE; ?>/membre/inscription">inscrivez-vous</a>
        pour pouvoir vois inscrire à la newsletter.
    </span>
    <?php } ?>
    <span>
        <p><?php echo $message ?></p>
    </span>
</div>
