<div id="panier">
    <h1>Votre Panier</h1>
    <?php
    if (!isset($_SESSION['panier'])) {
        echo 'Vous n\'avez actuellement aucun produit dans votre panier.';
    } else {
        ?>
    <table>
        <th>Salle</th>
        <th>Ville</th>
        <th>Capacité</th>
        <th>Date d'arrivée</th>
        <th>Date de départ</th>
        <th>Prix HT</th>
        <th>TVA</th>
        <th>Prix TTC</th>
        <th>Retirer</th>
        <?php
        foreach ($_SESSION['panier'] as $key => $value) {
            $produit = unserialize($_SESSION['panier'][$key]);
            echo '<tr>';
            echo '<td>' . $produit->getSalle()->getTitre() . '</td>';
            echo '<td>' . $produit->getSalle()->getVille() . '</td>';
            echo '<td>' . $produit->getSalle()->getCapacite() . '</td>';
            echo '<td>' . $produit->getArrivee()->format('d-m-Y') . '</td>';
            echo '<td>' . $produit->getDepart()->format('d-m-Y') . '</td>';
            echo '<td>' . $produit->getPrix() . '</td>';
            echo '<td>20%</td>';
            echo '<td>'. ($produit->getPrix()+($produit->getPrix()*20/100)) .'</td>';
            echo '<td><a href="' . RACINE_SITE . '/panier/retirer/'. $produit->getIdProduit() .'">X</a></td>';
            echo '</tr>';
        }
        ?>
    </table>
    <?php
        echo '<strong>Prix total TTC : ' . $totalTtc . ' €</strong><br />';
    ?>
    <form action="" method="post" name="valider">
        <label for="cgv" style="width: 350px;">
            J'accepte les conditions générales de vente
            (<a href="<?php echo RACINE_SITE;?>/informations/cgv">voir</a>)
        </label>
        <input type="checkbox" name="cgv" id="cgv" required="required" /><br />
        
        <label for="codePromo">Code Promo</label>
        <input type="text" name="codePromo" id="codePromo" value="<?php if(isset($_POST['codePromo'])) echo $_POST['codePromo']; ?>" /><br />
        <span class="error"><?php echo $error; ?></span><br />
        
        <input type="submit" name="payer" value="Payer" />
    </form>
    <?php  
        echo '<a href="' . RACINE_SITE . '/panier/vider">Vider le panier</a>';
    }
    ?>
    
</div>