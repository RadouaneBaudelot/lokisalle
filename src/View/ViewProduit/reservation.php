<div id="reservation">
    <h1>Réservation de salles</h1>
        <?php
        foreach ($produits as $produit) {
            echo '<div class="res">';
            echo '<img src="'.$produit->getSalle()->getPhoto().'" />';
            echo '<h3>' . $produit->getSalle()->getTitre() . '</h3><br />';
            echo '<span>' . $produit->getSalle()->getVille() . ', ' . $produit->getSalle()->getPays() . '</span><br />';
            echo '<span>Du ' . $produit->getArrivee()->format('d/m/Y') . ' au ' . $produit->getArrivee()->format('d/m/Y') . '</span><br />';
            echo '<span>' . $produit->getPrix() . ' euros pour ' . $produit->getSalle()->getCapacite() . ' personnes</span><br />';
            echo '<a href="'.RACINE_SITE.'/produit/detail/'.$produit->getIdProduit().'">Voir la fiche détaillée</a><br />';
            echo '<br />';
            if (isset($_SESSION['panier'][$produit->getIdProduit()])) { 
                echo '<p style="margin-left: 120px;">Produit ajouté au panier</p>';
            } elseif ($_SESSION['member']['status'] === 1 || $_SESSION['member']['status'] === 2) {
                ?>
                <div class="cart">    
                    <form action="" method="post" name="panier">
                        <input type="submit" name="ajouter<?php echo $produit->getIdProduit(); ?>" value="Ajouter au panier" /><br />
                    </form>
                </div>
                <?php
            } else {
                echo '<a href="'.RACINE_SITE.'/membre/connexion">Connectez-vous pour l\'ajouter au panier</a><br />';
            }
            echo '</div>';
        }
        ?>
</div>