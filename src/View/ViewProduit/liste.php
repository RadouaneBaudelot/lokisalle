<div id="gestion">
    <h1>Liste des produits</h1>
    <table>
        <th>Id</th>
        <th>Salle</th>
        <th>Adresse</th>
        <th>Cp</th>
        <th>Ville</th>
        <th>Pays</th>
        <th>Date d'arrivée</th>
        <th>Date de départ</th>
        <th>Prix</th>
        <th>Code promo</th>
        <th>Réduction</th>
        <th>Supprimer</th>
    <?php
    foreach ($produits as $produit) {
        echo '<tr>';
        echo '<td>' . $produit->getIdProduit() . '</td>';
        echo '<td>' . $produit->getSalle()->getTitre() . '</td>';
        echo '<td>' . $produit->getSalle()->getAdresse() . '</td>';
        echo '<td>' . $produit->getSalle()->getCp() . '</td>';
        echo '<td>' . $produit->getSalle()->getVille() . '</td>';
        echo '<td>' . $produit->getSalle()->getPays() . '</td>';
        echo '<td>' . $produit->getArrivee()->format('d/m/Y') . '</td>';
        echo '<td>' . $produit->getDepart()->format('d/m/Y') . '</td>';
        echo '<td>' . $produit->getPrix() . '</td>';
        echo '<td>';
        if (null !== $produit->getIdPromo()) {echo $produit->getPromo()->getCodePromo();} else { echo "-"; }
        echo '</td>';
        echo '<td>';
        if (null !== $produit->getIdPromo()) {echo $produit->getPromo()->getReduction();} else { echo "-"; }
        echo '</td>';
        echo '<td><a href="'.RACINE_SITE.'/produit/supprimer/'.$produit->getIdProduit().'">X</a></td>';
        echo '</tr>';
    }
    ?>
    </table>
</div>