<div id="detail">
    <h1>Détail d'une Salle</h1>
    <div id='general'>
        <h2><?php echo $produit->getSalle()->getTitre() . ' (' . $moyenne . '/10 de moyenne sur ' . $nbNotes . ' avis)'; ?></h2>
        <img src="<?php echo $produit->getSalle()->getPhoto(); ?>" />
        <p><?php echo $produit->getSalle()->getDescription(); ?></p><br />
        <p>Catégorie : <?php echo str_replace('cat', '', $produit->getSalle()->getCategorie()); ?> 
            - Capacité : <?php echo $produit->getSalle()->getCapacite(); ?></p>
    </div>
    <?php
    if (isset($_SESSION['panier'][$produit->getIdProduit()])) { 
            echo '<p style="text-align: center; margin-bottom: 20px;">Produit ajouté au panier</p>';
        } elseif ($_SESSION['member']['status'] === 1 || $_SESSION['member']['status'] === 2) {
    ?>
    <div class="button-panier">
        <form action="" method="post" name="panier">
            <input type="submit" name="ajouter" value="Ajouter au panier" /><br />
        </form>
    </div>
    <?php
    } else {
        echo '<a href="'.RACINE_SITE.'/membre/connexion">Connectez-vous pour l\'ajouter au panier</a><br />';
    }
    ?>
    <div id="infos">
        <h2>Informations Complémentaires</h2>
        <p>Adresse : <?php echo $produit->getSalle()->getAdresse(); ?></p>
        <p>Code Postal : <?php echo $produit->getSalle()->getCp(); ?></p>
        <p>Ville : <?php echo $produit->getSalle()->getVille(); ?></p>
        <p>Pays : <?php echo $produit->getSalle()->getPays(); ?></p>
        <p>Date d'arrivée : <?php echo $produit->getArrivee()->format('d-m-Y'); ?></p>
        <p>Date de départ : <?php echo $produit->getDepart()->format('d-m-Y'); ?></p>
        <p>Prix : <?php echo $produit->getPrix(); ?>*</p>
        <p>* Prix hors taxe</p>
    </div>
    <div id="avis">
        <h2>Avis</h2>
        <?php
        foreach ($avis as $commentaire) {
            echo '<p>' . $commentaire->getMembre()->getPseudo() . ', le ' . 
                    $commentaire->getDateCommentaire()->format('d-m-Y') . ' à ' . 
                    $commentaire->getDateCommentaire()->format('H:i:s') . ' (' . 
                    $commentaire->getNote(). '/10)</p>';
            echo '<p>' . $commentaire->getCommentaire() . '</p>';
            echo '<br /><hr />';
        }
        ?>
        <p>Ajouter un commentaire</p>
        <form action="" method="post" name="avis">
            <label for="commentaire">Commentaire :</label>
            <textarea id="commentaire" name="commentaire" cols="47" rows="4" value="<?php //echo $avis->getCommentaire() ?>" ></textarea><br />
            <span class="error"><?php echo $error->getErrCommentaire(); ?></span><br />
            
            <label for="note">Note :</label>
            <select id="note" name="note">
                <?php
                for ($i = 1; $i <11; $i++) {
                    echo '<option value="'.$i.'">' . $i . '</option>';
                }
                ?>
            </select>
            <span>/10</span><br />
            <span class="error"><?php echo $error->getErrNote(); ?></span><br />
            <?php 
            if (isset($_SESSION['member']['idMembre']) 
                    && in_array($_SESSION['member']['idMembre'], $membres)) { 
                echo 'Vous avez déjà déposé un commentaire pour cette salle.';
            } elseif ($_SESSION['member']['status'] === 0) {
                echo 'Vous devez <a href="'.RACINE_SITE.'/membre/connexion">vous connecter</a> pour déposer un commentaire';
            } else {
            ?>
            <input type="submit" name="commenter" value="Commenter" /><br />
            <?php } ?>
        </form>
        </div>
        <?php
        if (isset($_SESSION['panier'][$produit->getIdProduit()])) { 
                echo '<p style="text-align: center; margin-bottom: 20px;">Produit ajouté au panier</p>';
            } elseif ($_SESSION['member']['status'] === 1 || $_SESSION['member']['status'] === 2) {
        ?>
        <div class="button-panier">
            <form action="" method="post" name="panier">
                <input type="submit" name="ajouter" value="Ajouter au panier" /><br />
            </form>
        </div>
        <?php
        } else {
            echo '<a href="'.RACINE_SITE.'/membre/connexion">Connectez-vous pour l\'ajouter au panier</a><br />';
        }
        ?>
        <div id="suggestion">
            <h2>Autres suggestions</h2>
            <?php 
            if (empty($produits)) {
                echo '<p>Aucun résultat ne correspond à votre recherche</p>';
            }
            foreach ($produits as $produit) {
                echo '<div class="ressearch">';
                echo '<img src="'.$produit->getSalle()->getPhoto().'" />';
                echo '<h3>' . $produit->getSalle()->getTitre() . '</h3><br />';
                echo '<span>' . $produit->getSalle()->getVille() . ', ' . $produit->getSalle()->getPays() . '</span><br />';
                echo '<span>Du ' . $produit->getArrivee()->format('d/m/Y') . ' au ' . $produit->getArrivee()->format('d/m/Y') . '</span><br />';
                echo '<span>' . $produit->getPrix() . ' euros pour ' . $produit->getSalle()->getCapacite() . ' personnes</span><br />';
                echo '<a href="'.RACINE_SITE.'/produit/detail/'.$produit->getIdProduit().'">Voir la fiche détaillée</a><br />';
                echo '<br />';
                if ($_SESSION['member']['status'] === 1 || $_SESSION['member']['status'] === 2) {
                    echo '<a href="'.RACINE_SITE.'/panier/ajouter">Ajouter au panier</a><br />';
                } else {
                    echo '<a href="'.RACINE_SITE.'/membre/connexion">Connectez-vous pour l\'ajouter au panier</a><br />';
                }
                echo '</div>';
            }
            ?>
        </div>
</div>