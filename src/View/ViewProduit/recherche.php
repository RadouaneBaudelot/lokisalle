<div id="rechercher">
    <h1>Rechecher une Salle</h1>
    <div id="searchparams">
        <h2>Recherche</h2>
        <p style="text-align: center; margin-bottom: -20px;"><strong>OU</strong></p>
        <form action="" method="post" name="produit">
            <span class="datesearch">
            <label for="mois">Mois</label>
            <select id="mois" name="mois">
                <option value="01" <?php if ($month == '01') { echo 'selected="selected"'; } ?>>Janvier</option>
                <option value="02" <?php if ($month == '02') { echo 'selected="selected"'; } ?>>Février</option>
                <option value="03" <?php if ($month == '03') { echo 'selected="selected"'; } ?>>Mars</option>
                <option value="04" <?php if ($month == '04') { echo 'selected="selected"'; } ?>>Avril</option>
                <option value="05" <?php if ($month == '05') { echo 'selected="selected"'; } ?>>Mai</option>
                <option value="06" <?php if ($month == '06') { echo 'selected="selected"'; } ?>>Juin</option>
                <option value="07" <?php if ($month == '07') { echo 'selected="selected"'; } ?>>Juillet</option>
                <option value="08" <?php if ($month == '08') { echo 'selected="selected"'; } ?>>Août</option>
                <option value="09" <?php if ($month == '09') { echo 'selected="selected"'; } ?>>Septembre</option>
                <option value="10" <?php if ($month == '10') { echo 'selected="selected"'; } ?>>Octobre</option>
                <option value="11" <?php if ($month == '11') { echo 'selected="selected"'; } ?>>Novembre</option>
                <option value="12" <?php if ($month == '12') { echo 'selected="selected"'; } ?>>Décembre</option>
            </select><br />

            <label for="annee">Année</label>
            <select id="annee" name="annee" value="<?php echo $year; ?>">
                <?php
                for ($i = $year; $i <= ($year + 10); $i++) {
                    echo '<option value="'.$i.'">'. $i .'</option>';
                }
                ?>
            </select>
            </span>

            <span class="wordsearch">
            <label for="word">Mots clé</label>
            <input type="text" id="word" name="word" value="<?php if (isset($_POST['word'])) { echo $_POST['word']; } ?>" />
            </span>
            
            <input type="submit" name="rechercher" value="Rechercher" /><br />
        </form>
    </div>
    <?php if (!empty($_POST['rechercher'])) { ?>
    <div id="searchresults">
        <h2>Résultats de recherche</h2>
        <?php 
        if (empty($produits)) {
            echo '<p>Aucun résultat ne correspond à votre recherche</p>';
        }
        foreach ($produits as $produit) {
            echo '<div class="ressearch">';
            echo '<img src="'.$produit->getSalle()->getPhoto().'" />';
            echo '<h3>' . $produit->getSalle()->getTitre() . '</h3><br />';
            echo '<span>' . $produit->getSalle()->getVille() . ', ' . $produit->getSalle()->getPays() . '</span><br />';
            echo '<span>Du ' . $produit->getArrivee()->format('d/m/Y') . ' au ' . $produit->getArrivee()->format('d/m/Y') . '</span><br />';
            echo '<span>' . $produit->getPrix() . ' euros pour ' . $produit->getSalle()->getCapacite() . ' personnes</span><br />';
            echo '<a href="'.RACINE_SITE.'/produit/detail/'.$produit->getIdProduit().'">Voir la fiche détaillée</a><br />';
            echo '<br />';
            if (isset($_SESSION['panier'][$produit->getIdProduit()])) { 
                echo '<p style="margin-left: 120px;">Produit ajouté au panier</p>';
            } elseif ($_SESSION['member']['status'] === 1 || $_SESSION['member']['status'] === 2) {
                ?>
                <div class="cart">    
                    <form action="" method="post" name="panier">
                        <input type="submit" name="ajouter<?php echo $produit->getIdProduit(); ?>" value="Ajouter au panier" /><br />
                    </form>
                </div>
                <?php
            } else {
                echo '<a href="'.RACINE_SITE.'/membre/connexion">Connectez-vous pour l\'ajouter au panier</a><br />';
            }
            echo '</div>';
        }
        ?>
    <?php } ?>
    </div>
</div>