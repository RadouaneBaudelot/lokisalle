<div id="ajouterProduit">
    <h1>Ajouter un produit</h1>
    <form action="" method="post" name="produit">
        <label for="dateArrivee">Date d'arrivée</label>
        <input type="date" id="dateArrivee" name="dateArrivee" value="<?php echo $produit->getDateArrivee(); ?>" />
        <span class="error"><?php echo $error->getErrDateArrivee(); ?></span><br />
        
        <label for="dateDepart">Date de départ</label>
        <input type="date" id="dateDepart" name="dateDepart" value="<?php echo $produit->getDateDepart(); ?>" />
        <span class="error"><?php echo $error->getErrDateDepart(); ?></span><br />
        
        <label for="prix">Prix</label>
        <input type="text" id="prix" name="prix" value="<?php echo $produit->getPrix(); ?>" />
        <span class="error"><?php echo $error->getErrPrix(); ?></span><br />
        
        <label for="salleOption">Salle</label>
        <select id="salleOption" name="salleOption">
            <?php
            foreach ($salles as $salle) {
                echo '<option>' . $salle->getTitre() . '</option>';
            }
            ?>
        </select>
        <span class="error"><?php echo $error->getErrSalle(); ?></span><br />
        
        <label for="promoOption">Promo</label>
        <select id="promoOption" name="promoOption">
            <option>-</option>
            <?php
            foreach ($promos as $promo) {
                echo '<option>' . $promo->getCodePromo() . '</option>';
            }
            ?>
        </select><br />
        
        <input type="submit" name="ajouter" value="Ajouter un produit" /><br />
        
    </form>
</div>