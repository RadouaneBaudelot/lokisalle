<?php

class ProduitManager extends SuperManager
{
    public function creerProduit($dateArrivee, $dateDepart, $prix, $idSalle, $idPromo)
    {
        $req = $this->db->prepare("INSERT INTO produit(dateArrivee,  dateDepart,  prix,  etat,  idSalle,  idPromo)
                                   VALUES            (:dateArrivee, :dateDepart, :prix, :etat, :idSalle, :idPromo)");
        $req->bindValue(':dateArrivee', $dateArrivee,   PDO::PARAM_STR);
        $req->bindValue(':dateDepart',  $dateDepart,    PDO::PARAM_STR);
        $req->bindValue(':prix',        $prix,          PDO::PARAM_INT);
        $req->bindValue(':etat',        1,              PDO::PARAM_INT);
        $req->bindValue(':idSalle',     $idSalle,       PDO::PARAM_INT);
        $req->bindValue(':idPromo',     $idPromo,       PDO::PARAM_INT);
        
        $produit = $req->execute() or exit(print_r($this->db->errorInfo()));
        $req->closeCursor();

        return $produit;
    }
    
    public function isDateAvailable($dateArrivee, $dateDepart, $idSalle)
    {
        $req = $this->db->prepare("SELECT * FROM produit
                                   WHERE (dateArrivee BETWEEN :dateArrivee AND :dateDepart
                                   OR dateDepart BETWEEN :dateArrivee AND :dateDepart)
                                   AND idSalle = :idSalle");
        $req->bindValue(':dateArrivee', $dateArrivee,   PDO::PARAM_STR);
        $req->bindValue(':dateDepart',  $dateDepart,    PDO::PARAM_STR);
        $req->bindValue(':idSalle',     $idSalle,       PDO::PARAM_INT);
        
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->rowCount();
        
        if ($result >= 1) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    public function listeProduit()
    {
        $req = $this->db->prepare(" SELECT * FROM produit");
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->fetchAll(PDO::FETCH_CLASS, 'Produit');
        
        return $result;
    }
    
    public function acceuilProduit()
    {
        $req = $this->db->prepare(" SELECT * FROM produit
                                    WHERE dateArrivee > NOW()
                                    AND etat = 1
                                    ORDER BY idProduit DESC
                                    LIMIT 3");
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->fetchAll(PDO::FETCH_CLASS, 'Produit');
        
        return $result;
    }
    
    public function reservationProduit()
    {
        $req = $this->db->prepare(" SELECT * FROM produit
                                    WHERE dateArrivee > NOW()
                                    AND etat = 1
                                    ORDER BY dateArrivee ASC
                                    ");
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->fetchAll(PDO::FETCH_CLASS, 'Produit');
        
        return $result;
    }
    
    public function findByDate($year, $month)
    {
        $req = $this->db->prepare(" SELECT * FROM produit
                                    WHERE etat = 1
                                    AND (dateArrivee LIKE '%$year-$month%'
                                    OR dateDepart LIKE '%$year-$month%')
                                    AND dateArrivee > NOW()
                                    ORDER BY dateArrivee ASC
                                    ");
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->fetchAll(PDO::FETCH_CLASS, 'Produit');
        
        return $result;
    }
    
    public function findByWord($word)
    {
        $req = $this->db->prepare(" SELECT * FROM produit as p
                                    LEFT JOIN salle as s
                                    ON s.idSalle = p.idSalle
                                    WHERE p.etat = 1
                                    AND (s.ville LIKE '%$word%'
                                    OR s.titre LIKE '%$word%')
                                    AND dateArrivee > NOW()
                                    ORDER BY p.dateArrivee ASC
                                    ");
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->fetchAll(PDO::FETCH_CLASS, 'Produit');
        
        return $result;
    }
    
    public function suggestionProduit($year, $month, $ville, $id)
    {
        $req = $this->db->prepare(" SELECT * FROM produit as p
                                    LEFT JOIN salle as s
                                    ON s.idSalle = p.idSalle
                                    WHERE p.etat = 1
                                    AND ((p.dateArrivee LIKE '%$year-$month%'
                                    OR p.dateDepart LIKE '%$year-$month%')
                                    OR (s.ville LIKE '%$ville%'
                                    OR s.titre LIKE '%$ville%'))
                                    AND dateArrivee > NOW()
                                    AND p.idProduit NOT LIKE $id
                                    ORDER BY p.dateArrivee ASC
                                    LIMIT 4
                                    ");
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->fetchAll(PDO::FETCH_CLASS, 'Produit');
        
        return $result;
    }
}

/*DATE_FORMAT(dateArrivee, '%d/%m/%Y') AS dateArrivee,
                                           DATE_FORMAT(dateDepart, '%d/%m/%Y') AS dateDepart,
                                           prix, idSalle, idPromo, idProduit, etat*/
