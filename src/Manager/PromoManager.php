<?php

class PromoManager extends SuperManager
{
    public function creerPromo($codePromo, $reduction)
    {
        $req = $this->db->prepare('INSERT INTO promo(codePromo,  reduction)
                             VALUES                    (:codePromo,  :reduction)');
        $req->bindValue(':codePromo',   $codePromo,    PDO::PARAM_STR);
        $req->bindValue(':reduction',   $reduction,    PDO::PARAM_INT);
        
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $req->closeCursor();
    }
}