<?php

class AvisManager extends SuperManager
{
    public function creerAvis($commentaire, $note, $membre, $salle)
    {
        $req = $this->db->prepare("INSERT INTO avis(commentaire,   note, date,  idMembre, idSalle)
                                   VALUES          (:commentaire, :note, NOW(), :membre,  :salle)");
        $req->bindValue(':commentaire', $commentaire,   PDO::PARAM_STR);
        $req->bindValue(':note',        $note,          PDO::PARAM_INT);
        $req->bindValue(':membre',      $membre,        PDO::PARAM_INT);
        $req->bindValue(':salle',       $salle,         PDO::PARAM_INT);
        
        $avis = $req->execute() or exit(print_r($this->db->errorInfo()));
        $req->closeCursor();

        return $avis;
    }
    
    public function findSalleById($idSalle)
    {
        
    }
}