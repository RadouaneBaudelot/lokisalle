<?php

class PanierManager extends SuperManager
{
    public function insertOrder($montant, $idMembre, array $produits)
    {
        $req = $this->db->prepare("INSERT INTO commande( montant, date,   idMembre)
                                   VALUES              (:montant, NOW(), :idMembre)");
        $req->bindValue(':montant',     $montant,   PDO::PARAM_INT);
        $req->bindValue(':idMembre',    $idMembre,  PDO::PARAM_INT);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $lastInsertId = $this->db->lastInsertId();
        $req->closeCursor();
        
        foreach ($produits as $idProduit) {
            $stmt = $this->db->prepare("INSERT INTO details_commande( idCommande,  idProduit)
                                        VALUES                      (:idCommande, :idProduit)");
            $stmt->bindValue(':idCommande',  $lastInsertId,  PDO::PARAM_INT);
            $stmt->bindValue(':idProduit',   $idProduit,     PDO::PARAM_INT);
            $stmt->execute() or exit(print_r($this->db->errorInfo()));
            $stmt->closeCursor();
        }
        
    }
    
    public function insertOrderDetails($idCommande, $idProduit)
    {
        

        return $orderDetails;
    }
    
    public function disableOrderedProducts(array $products)
    {
        foreach ($products as $idProduit) {
            $req = $this->db->prepare("UPDATE produit
                                       SET  etat = 0
                                       WHERE idProduit = :idProduit");
            $req->bindValue(':idProduit', $idProduit, PDO::PARAM_INT);
            $req->execute() or exit(print_r($this->db->errorInfo()));
            $req->closeCursor();
        }
    }
}