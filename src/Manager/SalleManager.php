<?php

class SalleManager extends SuperManager
{
    public function creerSalle($pays, $ville, $adresse, $cp, $titre, $description, $photo, $capacite, $categorie)
    {
        $req = $this->db->prepare('INSERT INTO salle(pays,  ville,  adresse,  cp,  titre,  description,  photo,  capacite,  categorie)
                             VALUES                (:pays, :ville, :adresse, :cp, :titre, :description, :photo, :capacite, :categorie)');
        $req->bindValue(':pays',        $pays,          PDO::PARAM_STR);
        $req->bindValue(':ville',       $ville,         PDO::PARAM_STR);
        $req->bindValue(':adresse',     $adresse,       PDO::PARAM_STR);
        $req->bindValue(':cp',          $cp,            PDO::PARAM_INT);
        $req->bindValue(':titre',       $titre,         PDO::PARAM_STR);
        $req->bindValue(':description', $description,   PDO::PARAM_STR);
        $req->bindValue(':photo',       $photo,         PDO::PARAM_STR);
        $req->bindValue(':capacite',    $capacite,      PDO::PARAM_INT);
        $req->bindValue(':categorie',   $categorie,     PDO::PARAM_STR);
        
        $salle = $req->execute() or exit(print_r($this->db->errorInfo()));
        $req->closeCursor();

        return $salle;
    }
    
    public function listeSalle()
    {
        $req = $this->db->prepare(" SELECT * FROM salle");
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->fetchAll(PDO::FETCH_CLASS, 'Salle');
        
        return $result;
    }
}

