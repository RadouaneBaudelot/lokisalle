<?php

class MembreManager extends SuperManager
{
    /**
     * 
     * @param type $pseudo
     * @param type $mdp
     * @param type $nom
     * @param type $prenom
     * @param type $email
     * @param type $sexe
     * @param type $ville
     * @param type $cp
     * @param type $adresse
     * @return type
     */
    public function creerMembre($pseudo, $mdp, $nom, $prenom, $email, $sexe, $ville, $cp, $adresse)
    {
        $req = $this->db->prepare('INSERT INTO membre(pseudo,  mdp,  nom,  prenom,  email,  sexe,  ville,  cp,  adresse)
                             VALUES           (:pseudo, :mdp, :nom, :prenom, :email, :sexe, :ville, :cp, :adresse)');
        $req->bindValue(':pseudo',  $pseudo,       PDO::PARAM_STR);
        $req->bindValue(':mdp',     $mdp,          PDO::PARAM_STR);
        $req->bindValue(':nom',     $nom,          PDO::PARAM_STR);
        $req->bindValue(':prenom',  $prenom,       PDO::PARAM_STR);
        $req->bindValue(':email',   $email,        PDO::PARAM_STR);
        $req->bindValue(':sexe',    $sexe,         PDO::PARAM_STR);
        $req->bindValue(':ville',   $ville,        PDO::PARAM_STR);
        $req->bindValue(':cp',      $cp,           PDO::PARAM_INT);
        $req->bindValue(':adresse', $adresse,      PDO::PARAM_STR);
        
        $inscription = $req->execute() or exit(print_r($this->db->errorInfo()));
        $req->closeCursor();

        return $inscription;
    }
    
    public function modifierMembre($mdp, $nom, $prenom, $sexe, $ville, $cp, $adresse, $id)
    {
        $req = $this->db->prepare('UPDATE membre
                                   SET  mdp     =   :mdp,
                                        nom     =   :nom,
                                        prenom  =   :prenom,
                                        sexe    =   :sexe,
                                        ville   =   :ville,
                                        cp      =   :cp,
                                        adresse =   :adresse
                                   WHERE idMembre = :id');
        $req->bindValue(':mdp',     $mdp,          PDO::PARAM_STR);
        $req->bindValue(':nom',     $nom,          PDO::PARAM_STR);
        $req->bindValue(':prenom',  $prenom,       PDO::PARAM_STR);
        $req->bindValue(':sexe',    $sexe,         PDO::PARAM_STR);
        $req->bindValue(':ville',   $ville,        PDO::PARAM_STR);
        $req->bindValue(':cp',      $cp,           PDO::PARAM_INT);
        $req->bindValue(':adresse', $adresse,      PDO::PARAM_STR);
        $req->bindValue(':id',      $id,           PDO::PARAM_INT);
        
        $update = $req->execute() or exit(print_r($this->db->errorInfo()));
        $req->closeCursor();

        return $update;
    }
    
    public function modifierPseudo($pseudo, $id)
    {
        $req = $this->db->prepare('UPDATE membre
                                   SET pseudo = :pseudo
                                   WHERE idMembre = :id');
        $req->bindValue(':pseudo',  $pseudo,       PDO::PARAM_STR);
        $req->bindValue(':id',      $id,           PDO::PARAM_INT);
        
        $update = $req->execute() or exit(print_r($this->db->errorInfo()));
        $req->closeCursor();

        return $update;
    }
    
    public function modifierEmail($email, $id)
    {
        $req = $this->db->prepare('UPDATE membre
                                   SET email = :email
                                   WHERE idMembre = :id');
        $req->bindValue(':email',   $email, PDO::PARAM_STR);
        $req->bindValue(':id',      $id,    PDO::PARAM_INT);
        
        $update = $req->execute() or exit(print_r($this->db->errorInfo()));
        $req->closeCursor();

        return $update;
    }
    
    /**
     * 
     * @param type $pseudo
     * @return type
     */
    public function pseudoExists($pseudo)
    {
        $req = $this->db->prepare('SELECT * FROM membre WHERE pseudo = :pseudo');
        $req->bindValue(':pseudo',  $pseudo,       PDO::PARAM_STR);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $pseudoExists = $req->rowCount();
        $req->closeCursor();
        
        return $pseudoExists;
    }
    
    /**
     * 
     * @param type $email
     * @return type
     */
    public function emailExists($email)
    {
        $req = $this->db->prepare('SELECT * FROM membre WHERE email = :email');
        $req->bindValue(':email',  $email,       PDO::PARAM_STR);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $emailExists = $req->rowCount();
        $req->closeCursor();
        
        return $emailExists;
    }
    
    /**
     * 
     * @param type $pseudo
     * @return type
     */
    public function getMdpFromPseudo($pseudo)
    {
        $req = $this->db->prepare('SELECT mdp FROM membre WHERE pseudo= :pseudo');
        $req->bindValue(':pseudo',  $pseudo,    PDO::PARAM_STR);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $mdp = $req->fetch(PDO::FETCH_ASSOC);
        
        return $mdp['mdp'];
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function afficherMembre($id)
    {
        $req = $this->db->prepare('SELECT * FROM membre WHERE idMembre= :id');
        $req->bindValue(':id',  $id,    PDO::PARAM_INT);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        
        $membre = $req->fetchObject('Membre');
        
        return $membre;
    }
    
    /**
     * 
     * @param type $pseudo
     * @return type
     */
    public function getMembreByPseudo($pseudo)
    {
        $req = $this->db->prepare('SELECT * FROM membre WHERE pseudo= :pseudo');
        $req->bindValue(':pseudo',  $pseudo,    PDO::PARAM_STR);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        
        $membre = $req->fetchObject('Membre');
        
        return $membre;
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function getMembreById($id)
    {
        $req = $this->db->prepare('SELECT * FROM membre WHERE idMembre= :id');
        $req->bindValue(':id',  $id,    PDO::PARAM_STR);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        
        $membre = $req->fetchObject('Membre');
        
        return $membre;
    }
    
    /**
     * 
     * @param type $email
     * @param type $mdp
     */
    public function updatePassword($email, $mdp)
    {
        $req = $this->db->prepare('UPDATE membre
                                   SET mdp = :mdp
                                   WHERE email= :email');
        $req->bindValue(':mdp',     $mdp,       PDO::PARAM_STR);
        $req->bindValue(':email',   $email,     PDO::PARAM_STR);
        $req->execute() or exit(print_r($this->db->errorInfo()));
    }
    
    public function inscriptionNewsletter($id)
    {
        $req = $this->db->prepare('INSERT INTO newsletter(id_membre)
                             VALUES                      (:id)');
        $req->bindValue(':id', $id, PDO::PARAM_INT);
        
        $inscription = $req->execute() or exit(print_r($this->db->errorInfo()));
        $req->closeCursor();

        return $inscription;
    }
    
    public function membreAlreadyRegisteredToNl($id)
    {
        $req = $this->db->prepare('SELECT * FROM newsletter WHERE id_membre = :id');
        $req->bindValue(':id',  $id,       PDO::PARAM_INT);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $memberExists = $req->rowCount();
        $req->closeCursor();
        
        return $memberExists;
    }
    
    public function getMembersForNewsletters()
    {
        $req = $this->db->prepare('SELECT m.email, m.pseudo
                                   FROM newsletter AS nl
                                   LEFT JOIN membre AS m
                                   ON m.idMembre = nl.id_membre');
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $membre = $req->fetchAll(PDO::FETCH_ASSOC);
        return $membre;
    }
    
    public function countNewsletterRegisteredMembers()
    {
        $req = $this->db->prepare('SELECT * FROM newsletter');
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->rowCount();
        $req->closeCursor();
        
        return $result;
    }
    
    public function test()
    {
        $req = $this->db->prepare('SELECT * FROM membre LIMIT 1');
        $req->bindValue(':membre',  11,       PDO::PARAM_INT);
        $req->execute() or exit(print_r($this->db->errorInfo()));
        $result = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        
        return $result;
    }
}

