<?php

class Produit extends EntityBase
{
    protected $idProduit;
    protected $dateArrivee;
    protected $dateDepart;
    protected $prix;
    protected $etat;
    protected $idSalle;
    protected $idPromo;
    protected $salle;
    protected $promo;
    protected $arrivee;
    protected $depart;
    
    public function __construct()
    {
        parent::__construct($_POST);
        $produitManager = new ProduitManager();
        $this->salle = $produitManager->findById('salle', $this->getIdSalle());
        $this->promo = $produitManager->findById('promo', $this->getIdPromo());
        $this->arrivee = DateTime::createFromFormat('Y-m-d H:i:s', $this->getDateArrivee());
        $this->depart = DateTime::createFromFormat('Y-m-d H:i:s', $this->getDateDepart());
    }
    
    public function getIdProduit() {
        return $this->idProduit;
    }

    public function getDateArrivee() {
        return $this->dateArrivee;
    }

    public function getDateDepart() {
        return $this->dateDepart;
    }

    public function getPrix() {
        return $this->prix;
    }

    public function getEtat() {
        return $this->etat;
    }

    public function getIdSalle() {
        return $this->idSalle;
    }

    public function getIdPromo() {
        return $this->idPromo;
    }

    public function setIdProduit($idProduit) {
        $this->idProduit = $idProduit;
    }
    
    public function setDateArrivee($dateArrivee) {
        $this->dateArrivee = $dateArrivee;
    }
    
    public function setDateDepart($dateDepart) {
        $this->dateDepart = $dateDepart;
    }

    public function setPrix($prix) {
        $this->prix = $prix;
    }

    public function setEtat($etat) {
        $this->etat = $etat;
    }

    public function setIdSalle($idSalle) {
        $this->idSalle = $idSalle;
    }

    public function setIdPromo($idPromo) {
        $this->idPromo = $idPromo;
    }

    public function getSalle() {
        return $this->salle;
    }    
        
    public function getPromo() {
        return $this->promo;
    }
    
    public function getArrivee()
    {
        return $this->arrivee;
    }
    
    public function getDepart()
    {
        return $this->depart;
    }
}

