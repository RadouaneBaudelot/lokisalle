<?php

class Promo extends EntityBase
{
    protected $idPromo;
    protected $codePromo;
    protected $reduction;
    
    public function __construct()
    {
        parent::__construct($_POST);
    }
    
    public function getIdPromo() {
        return $this->idPromo;
    }

    public function getCodePromo() {
        return $this->codePromo;
    }

    public function getReduction() {
        return $this->reduction;
    }

    public function setIdPromo($idPromo) {
        $this->idPromo = $idPromo;
    }

    public function setCodePromo($codePromo) {
        $this->codePromo = $codePromo;
    }

    public function setReduction($reduction) {
        $this->reduction = $reduction;
    }
}