<?php

class Membre extends EntityBase
{
    protected $idMembre;
    protected $pseudo;
    protected $mdp;
    protected $nom;
    protected $prenom;
    protected $email; 
    protected $sexe;
    protected $ville;
    protected $cp;
    protected $adresse;
    protected $statut;
    
    const VISITOR   =   0;
    const MEMBER    =   1;
    const ADMIN     =   2;
        
    public function __construct()
    {
        parent::__construct($_POST);
    }
    
    /*-- SETTERS --*/
    public function setIdMembre($idMembre)
    {
        $this->idMembre = $idMembre;
    }
    
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
    }
    
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;
    }
    
    public function setNom($nom)
    {
        $this->nom = $nom;
    }
    
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }
    
    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
    }
    
    public function setVille($ville)
    {
        $this->ville = $ville;
    }
    
    public function setCp($cp)
    {
        $this->cp = $cp;
    }
    
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }
    
    public function setStatut($statut)
    {
        $this->statut = $statut;
    }
    /*-- FIN SETTERS --*/
    
    /*-- GETTERS --*/
    public function getIdMembre()
    {
        return $this->idMembre;
    }
    
    public function getPseudo()
    {
        return $this->pseudo;
    }
    
    public function getMdp()
    {
    	return $this->mdp;
    }
    
    public function getNom()
    {
    	return $this->nom;
    }
    
    public function getPrenom()
    {
    	return $this->prenom;
    }
    
    public function getEmail()
    {
    	return $this->email;
    }
    
    public function getSexe()
    {
    	return $this->sexe;
    }
    
    public function getVille()
    {
    	return $this->ville;
    }
    
    public function getCp()
    {
    	return $this->cp;
    }
    
    public function getAdresse()
    {
    	return $this->adresse;
    }
    
    public function getStatut()
    {
    	return $this->statut;
    }
    /*-- FIN GETTERS --*/
    
    
}

