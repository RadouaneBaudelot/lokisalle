<?php

class Salle extends EntityBase
{
    protected $idSalle;
    protected $pays;
    protected $ville;
    protected $adresse;
    protected $cp;
    protected $titre;
    protected $description;
    protected $photo;
    protected $capacite;
    protected $categorie;
    protected $avis;
    
    public function __construct()
    {
        parent::__construct($_POST);
        $salleManager = new SalleManager();
        $this->avis = $salleManager->findBy('avis', 'idSalle', $this->getIdSalle());
    }
    
    public function getIdSalle() {
        return $this->idSalle;
    }
    
    public function getPays() {
        return $this->pays;
    }

    public function getVille() {
        return $this->ville;
    }

    public function getAdresse() {
        return $this->adresse;
    }

    public function getCp() {
        return $this->cp;
    }

    public function getTitre() {
        return $this->titre;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getPhoto() {
        return $this->photo;
    }

    public function getCapacite() {
        return $this->capacite;
    }

    public function getCategorie() {
        return $this->categorie;
    }
    
    public function getAvis()
    {
        return $this->avis;
    }
    
    public function setIdSalle($idSalle) {
        $this->idSalle = $idSalle;
    }

    public function setPays($pays) {
        $this->pays = $pays;
    }

    public function setVille($ville) {
        $this->ville = $ville;
    }

    public function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    public function setCp($cp) {
        $this->cp = $cp;
    }

    public function setTitre($titre) {
        $this->titre = $titre;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setPhoto($photo) {
        $this->photo = $photo;
    }

    public function setCapacite($capacite) {
        $this->capacite = $capacite;
    }

    public function setCategorie($categorie) {
        $this->categorie = $categorie;
    }
}

