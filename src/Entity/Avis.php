<?php

class Avis extends EntityBase
{
    protected $idAvis;
    protected $commentaire;
    protected $note;
    protected $date;
    protected $idMembre;
    protected $idSalle;
    protected $membre;
    protected $dateCommentaire;
    
    public function __construct()
    {
        parent::__construct($_POST);
        $manager = new SuperManager;
        $this->membre = $manager->findById('membre', $this->getIdMembre());
        $this->dateCommentaire = DateTime::createFromFormat('Y-m-d H:i:s', $this->getDate());
    }
    
    public function getIdAvis() {
        return $this->idAvis;
    }

    public function getCommentaire() {
        return $this->commentaire;
    }

    public function getNote() {
        return $this->note;
    }

    public function getDate() {
        return $this->date;
    }

    public function getIdMembre() {
        return $this->idMembre;
    }

    public function getIdSalle() {
        return $this->idSalle;
    }
    
    public function getMembre() {
        return $this->membre;
    }
    
    public function getDateCommentaire() {
        return $this->dateCommentaire;
    }

    public function setIdAvis($idAvis) {
        $this->idAvis = $idAvis;
    }

    public function setCommentaire($commentaire) {
        $this->commentaire = $commentaire;
    }

    public function setNote($note) {
        $this->note = $note;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function setIdMembre($idMembre) {
        $this->idMembre = $idMembre;
    }

    public function setIdSalle($idSalle) {
        $this->idSalle = $idSalle;
    }
}