<?php

class MembreController extends SuperController
{
    protected $membreHandler;
    protected $membre;
    
    public function __construct() {
        $this->membreHandler = $this->getHandler();
        $this->membre = $this->getEntity();
    }
    
    public function inscriptionMembre()
    {
        if (isset($_POST['inscription'])) {
            extract($_POST);
            
            if ($this->membreHandler->checkDatasForRegistration($checkMdp, $checkEmail)) {
                $this->getManager()->creerMembre($pseudo, $mdp, $nom, $prenom, $email, $sexe, $ville, $cp, $adresse);
                $this->membreHandler->connect($pseudo);
                $this->redirect('membre', 'profil');
            }
        }

        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Membre/inscription', array('title'   =>  $title,
                                                  'action'  =>  $action,
                                                  'membre'  =>  $this->membre,
                                                  'error'   =>  $this->membreHandler));
    }
    
    public function modificationMembre()
    {
        if (isset($_SESSION['member']['idMembre']) && !empty($_SESSION['member']['idMembre'])) {
            $this->membre = $this->getManager()->getMembreById($_SESSION['member']['idMembre']);
            
            if (isset($_POST['modification'])) {
                extract($_POST);

                if ($this->membreHandler->checkDatasForUpdate($_SESSION['member']['pseudo'], $pseudo, $checkMdp, $_SESSION['member']['email'], $email, $checkEmail)) {
                    $this->getManager()->modifierMembre($mdp, $nom, $prenom, $sexe, $ville, $cp, $adresse, $_SESSION['member']['idMembre']);
                    
                    if ($_SESSION['member']['pseudo'] != $pseudo) {
                        $this->getManager()->modifierPseudo($pseudo, $_SESSION['member']['idMembre']);
                    }

                    if ($_SESSION['member']['email'] != $email) {
                        $this->getManager()->modifierPseudo($email, $_SESSION['member']['idMembre']);
                    }
                    
                    //On redéfinie la session
                    $_SESSION['member']['pseudo'] = $pseudo;
                    $_SESSION['member']['email'] = $email;

                    $this->redirect('membre', 'profil');
                }
                
            }
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Membre/modification', array('title'   =>  $title,
                                                  'action'  =>  $action,
                                                  'membre'  =>  $this->membre,
                                                  'error'   =>  $this->membreHandler));
    }
    
    public function connexionMembre()
    {
        if (isset($_POST['connexion'])) {
            extract($_POST);
            
            if ($this->membreHandler->checkDatasForConnection()) {
                $this->membreHandler->connect($pseudo);
                $this->redirect('membre', 'profil');
            }
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Membre/connexion', array('title'   =>  $title,
                                                  'action'  =>  $action,
                                                  'membre'  =>  $this->membre,
                                                  'error'   =>  $this->membreHandler));
    }
    
    public function deconnexionMembre()
    {
        session_destroy();
        $this->redirect('accueil', 'afficher');
    }
    
    public function profilMembre()
    {
        $membre = $this->getManager()->afficherMembre($_SESSION['member']['idMembre']);
                                
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Membre/profil', array('title'   => $title,
                                             'action'  => $action,
                                             'membre'  => $membre,
                                             'error'   => $this->membreHandler));
    }
    
    public function passMembre()
    {
        if (isset($_POST['pass'])) {
            extract($_POST);
            
            $emailBuilder = new EmailBuilder();
            $emailPass = new PassEmail();
            
            $emailBuilder->setText($emailPass->emailContent());
            $header = $emailBuilder->prepareHeader($email, 'contact@lokisalle.com', 'Lokisalle');
            $message = $emailBuilder->prepareMessage($email);
            
            // Send email and register new password
            $emailBuilder->sendEmail($email, $emailPass->getSubject(), $message, $header);
            $this->getManager()->updatePassword($email, $emailPass->getNewPassword());
            
            $this->redirect('membre', 'connexion');
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Membre/pass', array( 'title'   =>  $title,
                                            'action'  =>  $action,
                                            'membre'  =>  $this->membre,
                                            'error'   =>  $this->membreHandler));
    }
    
    public function contactMembre()
    {
        if (isset($_POST['contact'])) {
            extract($_POST);
            
            $emailBuilder = new EmailBuilder();
            
            $emailBuilder->setText($message);
            $header = $emailBuilder->prepareHeader($email, $sender, $name);
            $message = $emailBuilder->prepareMessage($email);
            
            $emailBuilder->sendEmail($email, $sujet, $message, $header);
            
            $this->redirect('accueil', 'afficher');
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Membre/contact', array( 'title'   =>  $title,
                                            'action'  =>  $action,
                                            'membre'  =>  $this->membre,
                                            'error'   =>  $this->membreHandler));
    }
    
    public function newsletterMembre()
    {
        $message = '';
        
        if ($_POST) {
            if ($this->getHandler()->checkInscriptionNewsletter($_SESSION['member']['idMembre'])) {
                $this->getManager()->inscriptionNewsletter($_SESSION['member']['idMembre']);
                $this->redirect('membre', 'profil');
            } else {
                $message = 'Vous êtes déjà inscrit à la newsletter';   
            } 
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Membre/newsletter', array( 'title'   =>  $title,
                                            'action'  =>  $action,
                                            'message' => $message));
    }
    
    public function envoyernlMembre()
    {
        $registerdMembers = $this->getManager()->countNewsletterRegisteredMembers();
        
        if (isset($_POST['envoyernl'])) {
            extract($_POST);
            
            $emailBuilder = new EmailBuilder();
            
            $list = $this->getManager()->getMembersForNewsletters();
            $name = 'Lokisalle';
            
            foreach ($list as $recipient) {
            $email = $recipient['email'];
                
            $emailBuilder->setText($message);
            $header = $emailBuilder->prepareHeader($email, $sender, $name);
            $message = $emailBuilder->prepareMessage($email);
            
            $emailBuilder->sendEmail($email, $sujet, $message, $header);
            }
            
            $this->redirect('accueil', 'afficher');
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Membre/envoyernl', array( 'title'   =>  $title,
                                                 'action'  =>  $action,
                                                 'abonnes' =>  $registerdMembers));
    }
    
    public function gestionMembre()
    {
        $membres = $this->getManager()->getAll('membre');
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Membre/gestion', array( 'title'   =>  $title,
                                                 'action'  =>  $action,
                                                'membres' => $membres));
    }
    
    public function supprimerMembre()
    {
        $this->getManager()->delete('membre', 'idMembre', $_GET['id']);
        
        $this->redirect('membre', 'gestion');
    }
}