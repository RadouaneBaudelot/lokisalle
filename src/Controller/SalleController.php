<?php

class SalleController extends SuperController
{
    private $salle;
    private $salleHandler;
    private $salleManager;
    
    public function __construct() {
        $this->salle = $this->getEntity();
        $this->salleHandler = $this->getHandler();
        $this->salleManager = $this->getManager();
    }
    
    public function ajouterSalle()
    {
        if (isset ($_POST['ajouter'])) {
            extract($_POST);
            
            if ($this->salleHandler->checkDatasForCreation()) {
            $upload = new Upload();
            $upload->saveFileInPath('photo', $titre);
            $uploadFile = $upload->uploadFileinDb('photo', $titre);
            
            $this->getManager()->creerSalle($pays, $ville, $adresse, $cp, $titre, $description, $uploadFile, $capacite, $categorie);
            }
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Salle/ajouter', array('title'   =>  $title,
                                             'action'  =>  $action,
                                             'salle' => $this->salle,
                                             'error' => $this->salleHandler));
    }
    
    public function gestionSalle()
    {
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Salle/gestion', array('title'   =>  $title,
                                               'action'  =>  $action));
    }
    
    public function listeSalle()
    {
        $salles = $this->salleManager->listeSalle();
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Salle/liste', array('title'   =>  $title,
                                               'action'  =>  $action,
                                               'salles' => $salles));
    }
    
    public function supprimerSalle()
    {
        $this->salleManager->delete('salle', 'idSalle', $_GET['id']);
        
        $this->redirect('salle', 'liste');
    }
}