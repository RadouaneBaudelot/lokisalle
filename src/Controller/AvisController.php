<?php

class AvisController extends SuperController
{
    protected $avis;
    protected $avisManager;
    protected $avisHandler;
    
    public function __construct()
    {
        $this->avis = new Avis();
        $this->avisManager = new AvisManager();
        $this->avisHandler = new AvisHandler();
    }
    
    public function gestionAvis()
    {
        $commentaires = $this->avisManager->getAll('avis');
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Avis/gestion', array('title'   =>  $title,
                                            'action'  =>  $action,
                                            'commentaires' => $commentaires,
                                            'avisManager' => $this->avisManager)
                     );
    }
    
    public function supprimerAvis()
    {
        $this->avisManager->delete('avis', 'idAvis', $_GET['id']);
        
        $this->redirect('avis', 'gestion');
    }
}