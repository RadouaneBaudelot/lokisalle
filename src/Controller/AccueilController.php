<?php

class AccueilController extends SuperController
{
    private $produitManager;
    
    public function __construct()
    {
        $this->produitManager = new ProduitManager();
    }
    
    public function afficherAccueil()
    {

        $produits = $this->produitManager->acceuilProduit();

        foreach($produits as $produit) {
            if (isset($_POST['ajouter'.$produit->getIdProduit()])) {
                $_SESSION['panier'][$produit->getIdProduit()] = serialize($produit);
            }
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Accueil/accueil', array('title' => $title,
                                               'action'  =>  $action,
                                               'produits' => $produits));
    }
}