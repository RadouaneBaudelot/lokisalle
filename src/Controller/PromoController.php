<?php

class PromoController extends SuperController
{
    protected $promo;
    protected $promoHandler;
    protected $promoManager;
    
    public function __construct() {
        $this->promo = $this->getEntity();
        $this->promoHandler = $this->getHandler();
        $this->promoManager = $this->getManager();
    }
    
    public function gestionPromo()
    {
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Promo/gestion', array('title'   =>  $title,
                                             'action'  =>  $action));
    }
    
    public function ajouterPromo()
    {
        if (isset($_POST['promo'])) {
            extract($_POST);
            
            if ($this->promoHandler->checkDatasForCreation()) {
                $this->promoManager->creerPromo($codePromo, $reduction);
                
                $this->redirect('promo', 'ajouter');
            }
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Promo/ajouter', array('title'   =>  $title,
                                             'action'  =>  $action,
                                             'promo'   => $this->promo,
                                             'error'   => $this->promoHandler));
    }
    
    public function afficherPromo()
    {
        $promos = $this->promoManager->getAll('promo');
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Promo/afficher', array('title'   =>  $title,
                                              'action'  =>  $action,
                                              'promos'   => $promos));
    }
    
    public function supprimerPromo()
    {
        $this->promoManager->delete('promo', 'idPromo', $_GET['id']);
        
        $this->redirect('promo', 'afficher');
    }
}