<?php

class PanierController extends SuperController
{
    public function afficherPanier()
    {
        if (isset($_SESSION['panier'])) {
            $sum = 0;
            foreach ($_SESSION['panier'] as $key => $value) {
                $produit = unserialize($_SESSION['panier'][$key]);
                $sum += $produit->getPrix();
            }
            
            $totalTtc = $sum+($sum*20/100);
            $err = 0;
            
            if (isset($_POST['payer']) && !empty($_POST['cgv'])) {
                $produits = [];
                $promos = [];
                foreach ($_SESSION['panier'] as $key => $value) {
                    $produit = unserialize($_SESSION['panier'][$key]);
                    $produits[] = $produit->getIdProduit();
                    if(!empty($_POST['codePromo'])) {
                        if ($produit->getPromo()) {
                            $promos[] = $produit->getPromo()->getCodePromo();
                        }
                    }
                }
                
                if (in_array($_POST['codePromo'], $promos)) {
                    $sum = $sum - $produit->getPromo()->getReduction();
                    $totalTtc = $sum+($sum*20/100);
                } else {
                    $error = 'Le code promo n\'est pas valide';
                    $err++;
                }
                
                if ($err == 0) {
                    $panierManager = $this->getManager();
                    $panierManager->insertOrder($totalTtc, $_SESSION['member']['idMembre'], $produits);
                    $panierManager->disableOrderedProducts($produits);
                    unset($_SESSION['panier']);    
                }
            }
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Panier/afficher', array('title' => $title,
                                               'action'  =>  $action,
                                               'error' => isset($error) ? $error : '',
                                               'totalTtc' => isset($totalTtc) ? $totalTtc : ''));
    }
    
    public function retirerPanier()
    {
        unset($_SESSION['panier'][$_GET['id']]);
        
        $this->redirect('panier', 'afficher');
    }
    
    public function viderPanier()
    {
        unset($_SESSION['panier']);
        
        $this->redirect('panier', 'afficher');
    }
}