<?php

class ProduitController extends SuperController
{
    protected $produitHandler;
    protected $produit;
    protected $produitManager;
    
    public function __construct() {
        $this->produitHandler = $this->getHandler();
        $this->produit = $this->getEntity();
        $this->produitManager = $this->getManager();
    }
    
    public function ajouterProduit()
    {
        $salleManager = new SalleManager();
        $salles = $salleManager->getAll('Salle');
        $salle = new Salle();
        
        $promoManager = new PromoManager();
        $promos = $promoManager->getAll('Promo');
        $promo = new Promo();
        
        if (isset($_POST['ajouter'])) {
            extract($_POST);
            
            $idSalle = $this->produitManager->getIdByCriteria('salle', 'titre', $salleOption);
            $idPromo = $this->produitManager->getIdByCriteria('promo', 'codePromo', $promoOption);
                        
            if ($idPromo === 0) {
                $idPromo = null;
            }
            
            if ($this->produitHandler->checkDatasForCreation($idSalle, $salleOption)) {
                $this->produitManager->creerProduit($dateArrivee, $dateDepart, $prix, $idSalle, $idPromo);
            }
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Produit/ajouter', array('title'   =>  $title,
                                               'action'  =>  $action,
                                               'produit' =>  $this->produit,
                                               'salles'  =>  $salles,
                                               'salle'   =>  $salle,
                                               'promos'  =>  $promos,
                                               'promo'   =>  $promo,
                                               'error'   =>  $this->produitHandler));
    }
    
    public function listeProduit()
    {
        $produits = $this->produitManager->listeProduit();
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Produit/liste', array('title'   =>  $title,
                                               'action'  =>  $action,
                                               'produits' => $produits));
    }
    
    public function supprimerProduit()
    {
        $this->produitManager->delete('produit', 'idProduit', $_GET['id']);
        
        $this->redirect('produit', 'liste');
    }
    
    public function gestionProduit()
    {
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Produit/gestion', array('title'   =>  $title,
                                               'action'  =>  $action));
    }
    
    public function reservationProduit()
    {
        $produits = $this->produitManager->reservationProduit();
        
        foreach($produits as $produit) {
            if (isset($_POST['ajouter'.$produit->getIdProduit()])) {
                $_SESSION['panier'][$produit->getIdProduit()] = serialize($produit);
            }
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Produit/reservation', array('title'   =>  $title,
                                                   'action'  =>  $action,
                                                   'produits' => $produits));
    }
    
    public function rechercheProduit()
    {
        $current = new \DateTime();
        $month = $current->format('m');
        $year = $current->format('Y');
        
        $produits = $this->produitManager->getAll('produit');
        
        foreach($produits as $produit) {
            if (isset($_POST['ajouter'.$produit->getIdProduit()])) {
                $_SESSION['panier'][$produit->getIdProduit()] = serialize($produit);
            }
        }
        
        if (isset($_POST['rechercher']) || isset($_POST['ajouter'])) {
            extract($_POST);
            
            if (!empty($word)) {
                $produits = $this->produitManager->findByWord($word);
            } else {
                 $produits = $this->produitManager->findByDate($annee, $mois);
            }
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Produit/recherche', array('title'   =>  $title,
                                                   'action'  =>  $action,
                                                   'produits' => isset($produits) ? $produits : '',
                                                   'month' =>$month,
                                                   'year' => $year));
    }
    
    public function detailProduit()
    {
        $produit = $this->produitManager->findById('produit', $_GET['id']);
        $avis = $this->produitManager->findBy('avis', 'idSalle', $produit->getSalle()->getIdSalle());
        
        $nbNotes = 0;
        $sum = 0;
        foreach ($avis as $value) {
            $sum += $value->getNote();
            $nbNotes++;
        }
        
        if ($nbNotes > 0) {
            $moyenne = round($sum / $nbNotes, 1);
        } else {
            $moyenne = ' -';
        }
        
        $membres = [];
        foreach ($avis as $val) {
            $membres[] = $val->getMembre()->getIdMembre();
        }
        
        $avisHandler = new AvisHandler();
        $avisManager = new AvisManager();
        
        if (isset($_POST['commenter'])) {
            extract($_POST);
            
            if ($avisHandler->checkDatas()) {
                $avisManager->creerAvis($commentaire, $note, $_SESSION['member']['idMembre'], $produit->getSalle()->getIdSalle());
                
                $this->redirect("produit", "detail/".$produit->getIdProduit());
            }
        }
        
        $produits = $this->produitManager->suggestionProduit($produit->getArrivee()->format('Y'),
                                                             $produit->getArrivee()->format('m'), 
                                                             $produit->getSalle()->getVille(), 
                                                             $produit->getIdProduit());
        
        if (isset($_POST['ajouter'])) {
            if (!$_SESSION['panier'][$produit->getIdProduit()]) {
                $_SESSION['panier'][$produit->getIdProduit()] = serialize($produit);
            }
        }
        
        $action = $this->getAction();
        $title = $this->getTitle();
        $this->render('Produit/detail', array('title'   =>  $title,
                                              'action'  =>  $action,
                                              'produit' => $produit,
                                              'avis'    => $avis,
                                              'moyenne' => $moyenne,
                                              'nbNotes' => $nbNotes,
                                              'membres' => $membres,
                                              'error'   => $avisHandler,
                                              'produits'=> $produits));
    }
}