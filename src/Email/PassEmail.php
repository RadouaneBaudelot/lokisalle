<?php

class PassEmail
{
    private $newPassword;
    private $subject;
    
    const SUBJECT = 'LokiSalle : Changement de mot de passe';
    
    public function __construct()
    {
        $this->subject = self::SUBJECT;
        $this->newPassword = $this->generateNewPassword();
    }
    
    public function emailContent()
    {
        $content = '<div>';
        $content .= '<p>Bonjour,</p>';
        $content .= '<p>Vous avez demandé un changement de mot de passe sur LokiSalle.</p>';
        $content .= '<p>Voici votre nouveau mot de passe : ' . $this->getNewPassword() . '</p>';
        $content .= '<a href="' .RACINE_SITE. '/membre/connexion">Connectez-vous à LokiSalle</a>';
        $content .= '<p>Cordialement,</p>';
        $content .= '<p>L\'équipe LokiSalle</p>';
        $content .= '</div>';
        
        return $content;
    }
    
    public function getSubject()
    {
        return $this->subject;
    }
    
    public function getNewPassword()
    {
        return $this->newPassword;
    }
    
    public function generateNewPassword()
    {
        return substr(md5(rand()), 0, 10);
    }
}
