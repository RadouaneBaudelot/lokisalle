<?php

class Config
{
    public function getConfig()
    {
        define('RACINE_FILE',   '/var/www/html');
        define('PROJECT',       '/lokisalle');

        if ($_SERVER['HTTP_HOST'] == 'dev.lokisalle.com') {
            define('RACINE_SITE', 'http://'.$_SERVER['HTTP_HOST']);
        } else {
            define('RACINE_SITE', 'http://www.griffedunet.com/lokisalle');
        }
        
        require_once RACINE_FILE.'/lokisalle/vendor/Class/Autoload.php';
        require_once RACINE_FILE.'/lokisalle/app/Routes.php';
        require_once RACINE_FILE.'/lokisalle/app/Router.php';
    }
    
}
