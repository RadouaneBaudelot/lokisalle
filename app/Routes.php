<?php
$membre = new Membre();

$standard = array(
                'membre'	=> array(
                    'deconnexion'	=> array(),
                    'contact' => array(),
                    'newsletter' => array()
                ),
                'article' 	=>array(
                    'afficher'	=> array(),
                    'creation'	=> array(),
                    'modification'	=> array('id' => ''),
                    'suppression'	=> array('id' => '')
                ),
                'produit' => array(
                    'reservation' => array(),
                    'recherche' => array(),
                    'detail' => array('id' => '')
                ),
                '404'		=> array(
                ),
                'accueil'   => array(
                    'afficher'  => array()
                ),
            );

if($_SESSION['member']['status'] == $membre::VISITOR)
{
    $visitor = array(
        'membre'	=> array(
            'inscription'   => array(),
            'connexion'	=> array(),
            'pass' => array()
            )
        );
    $routes = array_merge_recursive($standard, $visitor);
}
elseif($_SESSION['member']['status'] >= $membre::MEMBER)
{
    $member = array(
        'membre' => array(
            'profil' => array('id' => ''),
            'modification' => array()
            ),
        'panier' => array(
            'afficher' => array(),
            'vider' => array(),
            'retirer' => array('id' => '')
            )
        );

    if($_SESSION['member']['status'] == $membre::ADMIN)
    {
        $admin = array(
            'membre' => array(
                'envoyernl' => array(),
                'gestion' => array(),
                'supprimer' => array('id' => '')
                ),
            'salle' => array(
                'ajouter' => array(),
                'gestion' => array(),
                'liste' => array(),
                'supprimer' => array('id' => '')
                ),
            'produit' => array(
                'gestion' => array(),
                'ajouter' =>array(),
                'liste' => array(),
                'supprimer' =>array('id' => '')
            ),
            'avis' => array(
                'gestion' => array(),
                'supprimer' => array('id' => '')
            ),
            'promo' => array(
                'gestion' => array(),
                'ajouter' => array(),
                'afficher' => array(),
                'supprimer' => array()
                )
            );
        $routes = array_merge_recursive($standard, $member, $admin);
    }
    else
        $routes = array_merge_recursive($standard, $member);
}
else
    $routes = $standard;