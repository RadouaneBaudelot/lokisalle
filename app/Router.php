<?php

/* ROUTEUR vers le bon controller */

/* Accueil (index.php) */
if(empty($_GET))
{
	$controller = 'accueil';
        $action = 'afficher';
	require_once RACINE_FILE.'/lokisalle/src/Controller/AccueilController.php';
}
/* Fin Accueil (index.php) */

elseif(isset($_GET))
{
	extract($_GET);
	if(array_key_exists($controller, $routes) && !empty($action))
	{
		if(array_key_exists($action, $routes[$controller]) && !empty($action))
		{
			require_once RACINE_FILE.'/lokisalle/src/Controller/'.ucfirst($controller).'Controller.php';
			
		}
		else
		{
			$action = 'afficher';
            $controller = 'Error404';
            require_once RACINE_FILE.'/lokisalle/src/Controller/Error404Controller.php';
		}
	}
	else
	{
		$action = 'afficher';
        $controller = 'Error404';
        require_once RACINE_FILE.'/lokisalle/src/Controller/Error404Controller.php';
	}
}
else
{
	$action = 'afficher';
    $controller = 'Error404';
    require_once RACINE_FILE.'/lokisalle/src/Controller/Error404Controller.php';
}
$class = ucfirst($controller).'Controller';
$entity = new $class;	
$launch = $action.ucfirst($controller);
$entity->$launch();
/* FIN ROUTEUR vers le bon controller */

